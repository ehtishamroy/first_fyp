<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyadminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  ////////////////////itemController/////////////////////////////////////
    public function create()
    {
        return view('wikicomparemyadmin.items.additem');
    }
   
    public function itemlist()
    {
        return view('wikicomparemyadmin.items.itemlist');
    }

      ////////////////////End-itemController/////////////////////////////////////


      ////////////////////BLOGController/////////////////////////////////////

      public function createpost()
      {
          return view('wikicomparemyadmin.blog.addblog');
      }
      
      public function postlist()
      {
          return view('wikicomparemyadmin.blog.listpost');
      }

       ////////////////////ENDBLOGController/////////////////////////////////////
      
       ////////////////////CATEGORYController/////////////////////////////////////
       public function createcategory()
       {
           return view('wikicomparemyadmin.category.createcategory');
       }

       public function listcategory()
       {
           return view('wikicomparemyadmin.category.listcategory');
       }
       
     ////////////////////ENDCATEGORYController/////////////////////////////////////
       
}
