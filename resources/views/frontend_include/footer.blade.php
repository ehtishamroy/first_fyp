    <footer class="footer wow fadeInUp animated footer-3" data-wow-delay="900ms">
    	<div class="container-fluid custom-width">
    		<div class="row">
    			<div class="col-md-12 col-lg-2">
    				<!-- ===========================
    						Footer About
    					 =========================== -->
    				<div class="footer-about">
    					<a href="index.html" class="footer-about-logo">
    						<img  style="margin-top: -70px !important;" src="{{URL::to('assets/images/wikicomparelogoblue.png')}}" alt="Logo">
						
    					</a>
	    				<div class="footer-description">
	    					<p>Lorem ipsum dolor sit amet, anim id est laborum. Sed ut perspconsectetur, adipisci vam aliquam qua.</p>
	    				</div>
	    				<div class="wb-social-media">
						<a href="#" class="bh"><i class="fa fa-behance"></i></a>
						<a href="#" class="fb"><i class="fa fa-facebook-official"></i></a>
						<a href="#" class="db"><i class="fa fa-dribbble"></i></a>
						<a href="#" class="gp"><i class="fa fa-google-plus"></i></a>
						<a href="#" class="vn"><i class="fa fa-vine"></i></a>
						<a href="#" class="yt"><i class="fa fa-youtube-play"></i></a>
					</div>
    				</div>
    			</div>
    			<div class="col-6 col-md-3 col-lg-2 footer-nav">
    				<!-- ===========================
    						Festival Deals
    					 =========================== -->
    				<h6 class="footer-subtitle"> Deals</h6>
    				<ul>
    					<li><a href="#">LINK</a></li>
							<li><a href="#">LINK</a></li>
								<li><a href="#">LINK</a></li>
									<li><a href="#">LINK</a></li>
										<li><a href="#">LINK</a></li>

    				</ul>
    			</div>
    			<div class="col-6 col-md-3 col-lg-2 footer-nav">
    				<!-- ===========================
    						Top Stores
    					 =========================== -->
    				<div class="stores-list">
	    				<h6 class="footer-subtitle">Top Stores</h6>
	    				<ul>
	    			<li><a href="#">LINK</a></li>
						<li><a href="#">LINK</a></li>
							<li><a href="#">LINK</a></li>
								<li><a href="#">LINK</a></li>
									<li><a href="#">LINK</a></li>
	    					
	    				</ul>
    				</div>
    			</div>
    			<div class="col-6 col-md-3 col-lg-2 footer-nav">
    				<!-- ===========================
    						Need Help ?
    					 =========================== -->
    				<h6 class="footer-subtitle">Need Help ?</h6>
    				<ul>
    					<li><a href="#">LINK</a></li>
							<li><a href="#">LINK</a></li>

								<li><a href="#">LINK</a></li>
    				</ul>
    			</div>
    			<div class="col-6 col-md-3 col-lg-2 footer-nav">
    				<!-- ===========================
    						About
    					 =========================== -->
    				<h6 class="footer-subtitle">About</h6>
	    				<ul>
	    					<li><a href="#">LINK</a></li>
	    				
	    				</ul>
    			</div>
    
    		</div>
    	</div>
    </footer>
    <!-- =========================
        CopyRight
    ============================== -->
    <section class="copyright wow fadeInUp animated copyright-2" data-wow-delay="900ms">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-md-6">
	    			<div class="copyright-text">
	    				<p class="text-uppercase">COPYRIGHT &copy; 2021</p><a class="created-by">WIKICOMPARES</a>
	    			</div>
	    		</div>
	    		<div class="col-md-6">
	    	
	    		</div>
	    	</div>
	    </div>
	</section>



    <!-- =========================
    	Main Loding JS Script
    ============================== -->
    <script src="{{URL::to('js/jquery.min.js')}}"></script>
    <script src="{{URL::to('js/jquery-ui.js')}}"></script>
    <script src="{{URL::to('js/popper.js')}}"></script>
    <script src="{{URL::to('js/bootstrap.min.js')}}"></script>
    <script src="{{URL::to('js/jquery.counterup.min.js')}}"></script>
    <script src="{{URL::to('js/jquery.nav.js')}}"></script>
    <!-- <script src="js/jquery.nicescroll.js"></script> -->
    <script src="{{URL::to('js/jquery.rateyo.js')}}"></script>
    <script src="{{URL::to('js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{URL::to('js/jquery.sticky.js')}}"></script>
    <script src="{{URL::to('js/mobile.js')}}"></script>
    <script src="{{URL::to('js/lightslider.min.js')}}"></script>
    <script src="{{URL::to('js/owl.carousel.min.js')}}"></script>
    <script src="{{URL::to('js/circle-progress.min.js')}}"></script>
    <script src="{{URL::to('js/waypoints.min.js')}}"></script>
    
    <script src="{{URL::to('js/simplePlayer.js')}}"></script>
    
    <script src="{{URL::to('js/main.js')}}"></script>
  </body>

<!-- Mirrored from www.themeim.com/demo/blurb/demo/index-third-home.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Jan 2021 21:43:03 GMT -->
</html>
