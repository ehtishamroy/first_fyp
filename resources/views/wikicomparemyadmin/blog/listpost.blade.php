@extends('layouts.default')
@section('content')

<div class="breadcrumbbar">
    <div class="row align-items-center">
        <div class="col-md-8 col-lg-8">
            <h4 class="page-title">Post Detail</h4>
            <div class="breadcrumb-list">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">POST</a></li>
                    <li class="breadcrumb-item active" aria-current="page">BLOG</li>
                </ol>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="widgetbar">

                {{-- <button class="btn btn-primary">Publish</button> --}}
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumbbar -->
<!-- Start Contentbar -->
<div class="contentbar">
    <!-- Start row -->
    <div class="row">
        <!-- Start col -->
        <div class="col-lg-12">
            <div class="card m-b-30">
                <div class="card-header">
                    <h5 class="card-title"></h5>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="edit-btn">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Email ID</th>
                                    <th>Phone</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Mark</td>
                                    <td>CEO</td>
                                    <td>demo@example.com</td>
                                    <td>9898989898</td>
                                  <td style="white-space: nowrap; width: 15%;">
                                        <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                            <div class="btn-group btn-group-sm" style="float: none;">
                                            <button type="button" class="tabledit-edit-button btn btn-sm btn-info" style="float: none; margin: 5px;">
                                            
                                            <span class="ti-pencil"></span></button>
                                                        <button type="button" class="tabledit-delete-button btn btn-sm btn-info"
                                                    style="float: none; margin: 5px;"> <span  class="ti-trash"></span></button></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Larry</td>
                                    <td>Manager</td>
                                    <td>demo@example.com</td>
                                    <td>9797979797</td>
                                    <td style="white-space: nowrap; width: 15%;">
                                        <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                            <div class="btn-group btn-group-sm" style="float: none;">
                                            <button type="button" class="tabledit-edit-button btn btn-sm btn-info" style="float: none; margin: 5px;">

                                            <span class="ti-pencil"></span></button>
                                                        <button type="button" class="tabledit-delete-button btn btn-sm btn-info"
                                                    style="float: none; margin: 5px;"> <span  class="ti-trash"></span></button></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>John</td>
                                    <td>Employee</td>
                                    <td>demo@example.com</td>
                                    <td>9696969696</td>
                                  <td style="white-space: nowrap; width: 15%;">
                                        <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                            <div class="btn-group btn-group-sm" style="float: none;">
                                            <button type="button" class="tabledit-edit-button btn btn-sm btn-info" style="float: none; margin: 5px;">
                                            
                                            <span class="ti-pencil"></span></button>
                                                        <button type="button" class="tabledit-delete-button btn btn-sm btn-info"
                                                    style="float: none; margin: 5px;"> <span  class="ti-trash"></span></button></div>
                                        </div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- End row -->
</div>

@endsection