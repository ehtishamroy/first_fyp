@extends('layouts.default')
@section('content')

   <div class="breadcrumbbar">
                <div class="row align-items-center">
                    <div class="col-md-8 col-lg-8">
                        <h4 class="page-title">Item Detail</h4>
                        <div class="breadcrumb-list">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Item</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Add Item</li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="widgetbar">
                        
                            <button class="btn btn-primary">Publish</button>
                        </div>                        
                    </div>
                </div>          
            </div>
            <!-- End Breadcrumbbar -->
            <!-- Start Contentbar -->    
            <div class="contentbar">
                <!-- Start row -->
                <div class="row">
                    <!-- Start col -->
                    <div class="col-lg-8 col-xl-9">
                        <div class="card m-b-30">
                            <div class="card-header">
                                <h5 class="card-title">Blog Page</h5>
                            </div>
                            <div class="card-body">
                                <form>
                                    <div class="form-group row">
                                        <label for="productTitle" class="col-sm-12 col-form-label">Title</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control font-20" id="productTitle" placeholder="Title">
                                        </div>
                                    </div>                                     
                                     <div class="form-group row">
                                        <label class="col-sm-12 col-form-label">Content</label>
                                        <div class="col-sm-12">
                                         <textarea class="summernote" name="description"></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                               
                    </div>
                    <!-- End col -->
                    <!-- Start col -->
                    <div class="col-lg-4 col-xl-3">
                <div class="card m-b-30">
                            <div class="card-header">
                                <h5 class="card-title">Choose Category</h5>
                            </div>
                            <div class="card-body">
                                <select class="select2-single form-control" name="state">
                                    <option>Select</option>
                                    <optgroup label="Alaskan/Hawaiian Time Zone">
                                        <option value="AK">Alaska</option>
                                        <option value="HI">Hawaii</option>
                                    </optgroup>
                                    <optgroup label="Pacific Time Zone">
                                        <option value="CA">California</option>
                               
                                    </optgroup>
                                    <optgroup label="Mountain Time Zone">
                                        <option value="AZ">Arizona</option>
                                        <option value="CO">Colorado</option>
                                   
                                    </optgroup>
                                    <optgroup label="Central Time Zone">
                                        <option value="AL">Alabama</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="IL">Illinois</option>
                              
                                    </optgroup>
                                    <optgroup label="Eastern Time Zone">
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="FL">Florida</option>
                                     
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="card m-b-30">
                              <div class="row">
                    <!-- Start col -->
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="card m-b-30">
                            <div class="card-header">
                                <h5 class="card-title">Featured Image</h5>
                            </div>
                            <div class="card-body">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <!-- End col -->
                </div> 
                        </div>
                    </div>
                    <!-- End col -->
                </div>
                <!-- End row -->
            </div>

@endsection

