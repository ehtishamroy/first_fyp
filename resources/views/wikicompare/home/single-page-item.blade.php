<!doctype html>
<html class="no-js')}}" lang="">
  
<!-- Mirrored from www.themeim.com/demo/blurb/demo/product-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Jan 2021 21:43:59 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Blurb is Price Comparison, Product Review and Affiliate, Social Business, Multi vendor Store,  HTML Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->


    <!-- =========================
        Loding All Stylesheet
    ============================== -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/rateyo.css">
    <link rel="stylesheet" href="css/lightslider.min.css">
   	<link rel="stylesheet" href="css/jquery-ui.min.css">


    <link rel="stylesheet" href="css/megamenu.css">

    <!-- =========================
        Loding Main Theme Style
    ============================== -->
    <link rel="stylesheet" href="css/style.css">

    <!-- =========================
    	Header Loding JS Script
    ============================== -->
    <script src="{{URL::to('js/modernizr.js')}}"></script>
  </head>
  <body class="">
    <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="preloader"></div>

    <!-- =========================
        Header Top Section
    ============================== -->
    <section id="wd-header-top">
    	<div class="container">
    		<div class="row">
    			<div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-3">
				    <!-- =========================
				        Social Media List
				    ============================== -->
    				<div class="wb-social-media">
						<a href="#" class="bh"><i class="fa fa-behance"></i></a>
						<a href="#" class="fb"><i class="fa fa-facebook-official"></i></a>
						<a href="#" class="db"><i class="fa fa-dribbble"></i></a>
						<a href="#" class="gp"><i class="fa fa-google-plus"></i></a>
						<a href="#" class="vn"><i class="fa fa-vine"></i></a>
						<a href="#" class="yt"><i class="fa fa-youtube-play"></i></a>
					</div>
    			</div>
			    <div class="col-sm-12 col-md-4 col-lg-5 col-xl-6 d-none d-md-block">
			    	<div class="offer-text text-center">
			    		<p class="text-uppercase">WIKICOMPAREs</p>
			    	</div>
			    </div>
    			

    	</div>
    </section>

    <!-- =========================
        Header Top Section
    ============================== -->
    <section id="wd-header" class="d-flex align-items-center mob-sticky">
    	<div class="container">
    		<div class="row">
    		    <!-- =========================
					Mobile Menu 
				============================== -->
    		    <div class="order-2 order-sm-1 col-2 col-sm-2 col-md-4 d-block d-lg-none">
                    <div class="accordion-wrapper hide-sm-up">
                        <a href="#" class="mobile-open"><i class="fa fa-bars" ></i></a>
                        <!--Mobile Menu start-->

                        <ul id="mobilemenu" class="accordion">
                           <!-- <li class="mob-logo"><a href="index.html"><img src="{{URL::to('img/logo.png')}}"alt=""></a></li>-->
                            <li><a class="closeme" href="#"><i class="fa fa-times" ></i></a></li>
                            <li class="mob-logo"><a href="index.html"><img src="{{URL::to('img/logo.png')}}"alt=""></a></li>
                            
                            
                            <li>
                                <div class="link">Home<i class="fa fa-chevron-down"></i></div>
                                <ul class="submenu font-sky">
                                 
                                </ul>
                            </li>
                            <li>
                                <div class="link">Comparison Product <i class="fa fa-chevron-down"></i></div>
                                <ul class="submenu font-sky">
                                  
                                    
                                </ul>
                            </li>
                            <li>
                                <div class="link ">shop<i class="fa fa-chevron-down"></i></div>
                                <ul class="submenu">

                                </ul>
                            </li>

                            <li>
                                <div class="link">megamenu<i class="fa fa-chevron-down"></i></div>
                                <ul class="submenu ">
                                  <li><a href="shop-left-sidebar.html">Visual Phones</a></li>
					              <li><a href="shop-left-sidebar.html">Chinese phones</a></li>
					              <li><a href="shop-left-sidebar.html">Google Phones</a></li>
					              <li><a href="shop-left-sidebar.html">Video cameras</a></li>
					              <li><a href="shop-left-sidebar.html">Top Cameras</a></li>
					              <li><a href="shop-left-sidebar.html">Cheap Cameras</a></li>
					              <li><a href="shop-left-sidebar.html">Best Cameras</a></li>
					              <li><a href="shop-left-sidebar.html">Luxury Cameras</a></li>
					              <li><a href="shop-left-sidebar.html">Simple Cameras</a></li>
                                  <li><a href="shop-left-sidebar.html">Phone Electronice</a></li>
					              <li><a href="shop-left-sidebar.html">Phone Appereances</a></li>
					              <li><a href="shop-left-sidebar.html">Visual Phones</a></li>
					              <li><a href="shop-left-sidebar.html">Chinese phones</a></li>
					              <li><a href="shop-left-sidebar.html">Google Phones</a></li>
					              <li><a href="shop-left-sidebar.html">Cheap Phones</a></li>
					              <li><a href="shop-left-sidebar.html">Luxury phones</a></li>
					              <li><a href="shop-left-sidebar.html">Simple phones</a></li>
                                  <li><a href="shop-left-sidebar.html">Camera Electronice</a></li>
					              <li><a href="shop-left-sidebar.html">Camera Appereances</a></li>
					              <li><a href="shop-left-sidebar.html">DSLR</a></li>
					              <li><a href="shop-left-sidebar.html">Video cameras</a></li>
					              <li><a href="shop-left-sidebar.html">Top Cameras</a></li>
					              <li><a href="shop-left-sidebar.html">Cheap Cameras</a></li>
					              <li><a href="shop-left-sidebar.html">Best Cameras</a></li>
					              <li><a href="shop-left-sidebar.html">Luxury Cameras</a></li>
					              <li><a href="shop-left-sidebar.html">Simple Cameras</a></li>
                                </ul>
                                
                            </li>
                            <li>
                                <div class="link">Reviews<i class="fa fa-chevron-down"></i></div>
                                <ul class="submenu">
                                    <li><a href="product-details-review-history.html">Product History</a></li>
                                    <li><a href="product-details-single-review.html">Single Review</a></li>
                                    <li><a href="review-left-sidebar.html">Review Left Sidebar</a></li>
                                    <li><a href="review-right-sidebar.html">Review Right Sidebar</a></li>
                                </ul>
                            </li>
                            <li>
                                <div class="link">Blog<i class="fa fa-chevron-down"></i></div>
                                <ul class="submenu">
                                    <li><a href="blog-full-grid.html">Blog Full Grid</a></li>
                                    <li><a href="blog-two-grid.html">Blog Two Grid</a></li>
                                    <li><a href="blog-three-grid.html">Blog Three Grid</a></li>
                                    <li><a href="blog-four-grid.html">Blog Four Grid</a></li>
                                    <li><a href="blog-four-grid-left-sidebar.html">Blog Four Grid Left Sidebar</a></li>
                                    <li><a href="blog-four-grid-right-sidebar.html">Blog Four Grid Right Sidebar</a></li>
                                    <li><a href="single-blog-with.html">Single Blog</a></li>
                                    <li><a href="single-blog-with-add.html">Single Blog With Add</a></li>
                                </ul>
                            </li>
                            <li class="out-link"><a class="" href="contact-us.html">Contact</a></li>
                            <li class="out-link"><a class="" href="coupon.html">Coupon</a></li>

                        </ul>
                        <!--Mobile Menu end-->
                    </div>
    		    </div><!--Mobile menu end-->
				    
    			<div class="order-1 order-sm-2  col-12 col-sm-4 col-md-4 col-lg-2 col-xl-2">
    				<div class="blrub-logo">
	    				<a href="index.html">
	    					<img src="{{URL::to('img/logo.png')}}"alt="Logo">
	    				</a>
    				</div>
    			</div>

				<!-- =========================
					 Search Box  Show on large device
				============================== -->
				<div class="col-12 order-lg-2 col-md-5 col-lg-6 col-xl-5 d-none d-lg-block">
				    <div class="input-group wd-btn-group header-search-option">
                        <input type="text" class="form-control blurb-search" placeholder="Search ..." aria-label="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary wd-btn-search" type="button">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </span>
                    </div>
				</div>

				<!-- =========================
					 Login and My Acount 
				============================== -->
				<div class="order-3 order-sm-3 col-10 col-sm-6 col-lg-4 col-md-4 col-xl-5">
					<!-- =========================
						 User Account Section
					============================== -->
                        <div class="acc-header-wraper">
						    <div class="account-section">
								<button class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg2">
									<i class="fa fa-sign-in" aria-hidden="true"></i><span>Login/Register</span> 
								</button>

								<div class="modal fade bd-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="container">
												<div class="row text-left">
													<div class="col-md-12 p0">

														<div class="modal-tab-section wd-modal-tabs">
															<ul class="nav nav-tabs wd-modal-tab-menu text-center" id="myTab" role="tablist">
																<li class="nav-item">
																	<a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-expanded="true">Login</a>
																</li>
																<li class="nav-item">
																	<a class="nav-link" id="sign-up-tab" data-toggle="tab" href="#sign-up" role="tab" aria-controls="sign-up">Sign up</a>
																</li>
															</ul>
															<div class="tab-content" id="myTabContent">
																<div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">

																<div class="row">
																	<div class="col-md-6 p0 brand-description-area">
																		<img src="{{URL::to('img/login-img-1.jpg')}}" class="img-fluid" alt="">
																		<div class="brand-description">
																			<div class="brand-logo">
																				<img src="{{URL::to('img/logo.png')}}" alt="Logo">
																			</div>
																			<div class="modal-description">
																				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod teoccaecvoluptatem.</p>
																			</div>
																			<ul class="list-unstyled">
																				<li class="media">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																				<li class="media my-4">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																				<li class="media">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="col-md-12 col-lg-6 p0">
																		<div class="login-section text-center">
																			<div class="social-media">
																				<a href="#" class="facebook-bg"><i class="fa fa-facebook" aria-hidden="true"></i> Login</a>
																				<a href="#" class="twitter-bg"><i class="fa fa-twitter" aria-hidden="true"></i> Login</a>
																				<a href="#" class="google-bg"><i class="fa fa-google-plus" aria-hidden="true"></i> Login</a>
																			</div>
																			<div class="login-form text-left">
																				<form>
																					<div class="form-group">
																						<label for="exampleInputEmail-login">User name</label>
																						<input type="text" class="form-control" id="exampleInputEmail-login" placeholder="John mist |">
																					</div>
																					<div class="form-group">
																						<label for="exampleInputPassword-login-pass-2">Password</label>
																						<input type="password" class="form-control" id="exampleInputPassword-login-pass-2" placeholder="*** *** ***">
																					</div>
																					<button type="submit" class="btn btn-primary wd-login-btn">LOGIN</button>

																					<div class="form-check">
																						<label class="form-check-label">
																							<input type="checkbox" class="form-check-input">
																							Save this password
																						</label>
																					</div>
																					
																					<div class="wd-policy">
																						<p>
																							By Continuing. I conferm that i have read and userstand the <a href="#">terms of uses</a> and <a href="#">Privacy Policy</a>. 
																							Don’t have an account? <a href="#" class="black-color"><strong><u>Sign up</u></strong></a>
																						</p>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>

																</div>
																<div class="tab-pane fade" id="sign-up" role="tabpanel" aria-labelledby="sign-up-tab">

																<div class="row">
																	<div class="col-md-6 p0 brand-login-section">
																		<img src="{{URL::to('img/login-img-2.jpg')}}"class="img-fluid" alt="">
																		<div class="brand-description">
																			<div class="brand-logo">
																				<img src="{{URL::to('img/logo.png')}}"alt="Logo">
																			</div>
																			<div class="modal-description">
																				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod teoccaecvoluptatem.</p>
																			</div>
																			<ul class="list-unstyled">
																				<li class="media">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																				<li class="media my-4">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																				<li class="media">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="col-md-6 p0">
																		<div class="sign-up-section text-center">
																			<div class="login-form text-left">
																				<form>
																					<div class="form-group">
																						<label for="exampleInputname1">First name</label>
																						<input type="text" class="form-control" id="exampleInputname1" placeholder="First Name">
																					</div>
																					<div class="form-group">
																						<label for="exampleInputname2">Last name</label>
																						<input type="text" class="form-control" id="exampleInputname2" placeholder="Last Name">
																					</div>
																					<div class="form-group">
																						<label for="exampleInputEmail-sign-up">Email</label>
																						<input type="text" class="form-control" id="exampleInputEmail-sign-up" placeholder="Enter you email ...">
																					</div>
																					<div class="form-group">
																						<label for="exampleInputPassword-login-pass">Password</label>
																						<input type="password" class="form-control" id="exampleInputPassword-login-pass" placeholder="*** *** ***">
																					</div>
																					<button type="submit" class="btn btn-primary wd-login-btn">Sign Up</button>
																					
																					<div class="wd-policy">
																						<p>
																							By Continuing. I conferm that i have read and userstand the <a href="#">terms of uses</a> and <a href="#">Privacy Policy</a>. 
																							Don’t have an account? <a href="#" class="black-color"><strong><u>Sign up</u></strong></a>
																						</p>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>

																</div>
															</div>
														</div>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="serch-wrapper">
                                <div class="search">
                                    <input class="search-input" placeholder="Search" type="text">
                                    <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
						

						<!-- =========================
							 Cart Out 
						============================== -->
						
							<div class="header-cart">
							    <a href="coupon.html" class="coupon-save"><i class="fa fa-star-o" aria-hidden="true"></i>
							    <span class="count">5</span>
							    </a>
							    
								<a class="header-wishlist" href="wishlist.html">
									<i class="fa fa-heart-o" aria-hidden="true"></i>
									<span class="count">8</span>
								</a>
								<div class="dropdown wd-compare-btn">
								  <button class="btn btn-secondary dropdown-toggle compare-btn" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    <i class="fa fa-balance-scale"></i>

								  </button>
								  <span class="count">9</span>
								  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
								  	<div class="wd-item-list">
									  	<div class="media">
											<img class="d-flex mr-3" src="{{URL::to('img/cart-img-1.jpg')}}"alt="cart-img">
											<div class="media-body">
												<h6 class="mt-0 list-group-title">Voyage Yoga Bag</h6>
												<div class="rating">
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
												</div>
												<div class="cart-price">$59</div>
											</div>
										</div>
									  	<div class="media">
											<img class="d-flex mr-3" src="{{URL::to('img/cart-img-2.jpg')}}"alt="cart-img">
											<div class="media-body">
												<h6 class="mt-0 list-group-title">Voyage Yoga Bag</h6>
												<div class="rating">
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
												</div>
												<div class="cart-price">$59</div>
											</div>
										</div>
									  	<div class="media">
											<img class="d-flex mr-3" src="{{URL::to('img/cart-img-1.jpg')}}"alt="cart-img">
											<div class="media-body">
												<h6 class="mt-0 list-group-title">Voyage Yoga Bag</h6>
												<div class="rating">
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
												</div>
												<div class="cart-price">$59</div>
											</div>
										</div>
									  	<div class="media">
											<img class="d-flex mr-3" src="{{URL::to('img/cart-img-2.jpg')}}"alt="cart-img">
											<div class="media-body">
												<h6 class="mt-0 list-group-title">Voyage Yoga Bag</h6>
												<div class="rating">
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
												</div>
												<div class="cart-price">$59</div>
											</div>
										</div>
									  	<div class="media">
											<img class="d-flex mr-3" src="{{URL::to('img/cart-img-1.jpg')}}"alt="cart-img">
											<div class="media-body">
												<h6 class="mt-0 list-group-title">Voyage Yoga Bag</h6>
												<div class="rating">
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
													<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
												</div>
												<div class="cart-price">$59</div>
											</div>
										</div>
									</div>
								  	<div class="media text-center">
										<a href="compare-products.html" class="btn btn-primary go-compare-page">Compare 
										<i class="fa fa-arrow-right" aria-hidden="true"></i></a>
									</div>
								  </div>
								</div>
							</div>
				    </div>
						
				</div>
            </div><!--Row End-->
    	</div><!--Container End-->
    </section><!--Section End-->

    <!-- =========================
        Main Menu Section
    ============================== -->
    <section id="main-menu" class="sticker-nav">
    	<div class="container">
    		<div class="row">
    			<div class="col-2 col-md-6 col-lg-12 ">
					<div class="menu-container wd-megamenu">
                        <div class="menu">
                            <ul class="wd-megamenu-ul">
                                <li><a href="{{url('/')}}" class="main-menu-list"><i class="fa fa-home" aria-hidden="true"></i> Home <i class="fa fa-angle-down angle-down" aria-hidden="true"></i></a>
                                    <ul class="single-dropdown">
                                   
                                    </ul>
                                </li>
                                <li><a href="{{url('/')}}" class="main-menu-list">Comparison Product <i class="fa fa-angle-down angle-down" aria-hidden="true"></i></a>
                                    <ul class="single-dropdown">
                                    
                                    </ul>
                                </li>
                                <li><a href="shop-left-sidebar.html" class="main-menu-list">Shop <i class="fa fa-angle-down angle-down" aria-hidden="true"></i></a>
                                    <ul class="single-dropdown">
                                        <li><a href="shop-left-sidebar.html">Shop Page</a></li>
                                        <li><a href="shop-right-sidebar.html">Shop Right Sidebar</a></li>
                                        <li><a href="shop-left-sidebar-full-grid.html">Shop Left Sidebar Full Grid</a></li>
                                        <li><a href="shop-right-sidebar-full-grid.html">Shop Right Sidebar Full Grid</a></li>
                                        <li><a href="product-details.html">Product Details</a></li>
                                        <li><a href="product-details-scroll.html">Product Details v2</a></li>
                                        <li><a href="wishlist.html">Wishlist View</a></li>
                                    </ul>
                                </li>
                                <li class="pos-inherit"><a href="shop-left-sidebar.html" class="main-menu-list ">Shortcode <i class="fa fa-angle-down angle-down" aria-hidden="true"></i></a>
                                    <ul class="single-dropdown megamenu">
                                        <li><a href="#" class="menu-subtitle">Shortcode One</a>
                                            <ul class="sub-menu-list">
                                                <li><a href="blurb-blog.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i> blurb-blog</a></li>
                                                <li><a href="blurb-call-to-action.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-call-to-action</a></li>
                                                <li><a href="blurb-carousel.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-carousel</a></li>
                                                <li><a href="blurb-compare-products.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-compare-products</a></li>
                                                <li><a href="blurb-footer.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-footer</a></li>
                                                <li><a href="blurb-counterup.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb Count up</a></li>
                                                <li><a href="404.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>404 page</a></li>
                                                
                                            </ul>
                                        </li>
                                        <li><a href="#" class="menu-subtitle">Shortcode Two</a>
                                            <ul class="sub-menu-list">
                                                
                                                <li><a href="blurb-product.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-product</a></li>
                                                <li><a href="blurb-reveiw.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-reveiw</a></li>
                                                <li><a href="blurb-service.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-service</a></li>
                                                <li><a href="blurb-widgetstyle-1.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-widgetstyle-1</a></li>
                                                <li><a href="blurb-social.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb social icon</a></li>
                                                <li><a href="blurb-subscribe.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb Subscribe</a></li>
                                                <li><a href="coming-soon.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Coming Soon</a></li>
                                                
                                            </ul>
                                        </li>
                                        <li><a href="#" class="menu-subtitle">Shortcode three</a>
                                            <ul class="sub-menu-list">
                                                <li><a href="blurb-widgetstyle-2.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-widgetstyle-2</a></li>
                                                <li><a href="blurb-widgetstyle-3.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-widgetstyle-3</a></li>
                                                <li><a href="blurb-widgetstyle-4.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-widgetstyle-4</a></li>
                                                <li><a href="blurb-widgetstyle-5.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-widgetstyle-5</a></li>
                                                <li><a href="blurb-widgetstyle-6.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>blurb-widgetstyle-6</a></li>
                                                
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="product-details-scroll.html" class="add-img"><img src="{{URL::to('img/nav-img-1.jpg')}}"class="figure-img img-fluid" alt="Product Img"></a>
                                            <a href="product-details-scroll.html" class="add-img"><img src="{{URL::to('img/nav-img-2.jpg')}}"class="figure-img img-fluid" alt="Product Img"></a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="review-left-sidebar.html" class="main-menu-list">REVIEWS</a>
                                    <ul class="single-dropdown">
                                        <li><a href="product-details-review-history.html">Product History</a></li>
                                        <li><a href="product-details-single-review.html">Single Review</a></li>
                                        <li><a href="review-left-sidebar.html">Review Left Sidebar</a></li>
                                        <li><a href="review-right-sidebar.html">Review Right Sidebar</a></li>
                                    </ul>
                                </li>
                                <li><a href="blog-four-grid-left-sidebar.html" class="main-menu-list">Blog <i class="fa fa-angle-down angle-down" aria-hidden="true"></i></a>
                                    <ul class="single-dropdown">
                                        <li><a href="blog-full-grid.html">Blog Full Grid</a></li>
                                        <li><a href="blog-two-grid.html">Blog Two Grid</a></li>
                                        <li><a href="blog-three-grid.html">Blog Three Grid</a></li>
                                        <li><a href="blog-four-grid.html">Blog Four Grid</a></li>
                                        <li><a href="blog-four-grid-left-sidebar.html">Blog Four Grid Left Sidebar</a></li>
                                        <li><a href="blog-four-grid-right-sidebar.html">Blog Four Grid Right Sidebar</a></li>
                                        <li><a href="single-blog-with.html">Single Blog</a></li>
                                        <li><a href="single-blog-with-add.html">Single Blog With Add</a></li>
                                    </ul>
                                </li>
                                <li><a href="coupon.html" class="main-menu-list">Coupon</a></li>
                            </ul>
                        </div>
					</div>
    			</div>
				<div class="col-6 col-md-4 col-lg-5 text-right ext-right p0  d-none ">
					<div class="account-and-search">
						<div class="account-section">
							<button class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">
								<i class="fa fa-sign-in" aria-hidden="true"></i>
							</button>

							<div class="modal wd-ph-modal fade bd-example-modal-lg" tabindex="-1" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="container">
											<div class="row text-left">
												<div class="col-md-12 p0">

													<div class="modal-tab-section wd-modal-tabs">
														<ul class="nav nav-tabs wd-modal-tab-menu text-center" id="myTab-account" role="tablist">
															<li class="nav-item">
																<a class="nav-link active" id="login-tab-2" data-toggle="tab" href="#login-2" role="tab" aria-controls="login-2" aria-expanded="true">Login</a>
															</li>
															<li class="nav-item">
																<a class="nav-link" id="sign-up-tab-2" data-toggle="tab" href="#sign-up-2" role="tab" aria-controls="sign-up-2">Sign up</a>
															</li>
														</ul>
														<div class="tab-content" id="myTabContent-account">
															<div class="tab-pane fade show active" id="login-2" role="tabpanel" aria-labelledby="login-tab-2">

																<div class="row">
																	<div class="col-md-6 p0 brand-description-area">
																		<img src="{{URL::to('img/login-img-1.jpg')}}"class="img-fluid" alt="">
																		<div class="brand-description">
																			<div class="brand-logo">
																				<img src="{{URL::to('img/logo.png')}}"alt="Logo">
																			</div>
																			<div class="modal-description">
																				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod teoccaecvoluptatem.</p>
																			</div>
																			<ul class="list-unstyled">
																				<li class="media">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																				<li class="media my-4">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																				<li class="media">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="col-12 col-md-12 col-lg-6 p0">
																		<div class="login-section text-center">
																			<div class="social-media ph-social-media">
																				<a href="#" class="facebook-bg"><i class="fa fa-facebook" aria-hidden="true"></i> Login</a>
																				<a href="#" class="twitter-bg"><i class="fa fa-twitter" aria-hidden="true"></i> Login</a>
																				<a href="#" class="google-bg"><i class="fa fa-google-plus" aria-hidden="true"></i> Login</a>
																			</div>
																			<div class="login-form text-left">
																				<form>
																					<div class="form-group">
																						<label for="exampleInputEmail1">User name</label>
																						<input type="text" class="form-control" id="exampleInputEmail1" placeholder="John mist |">
																					</div>
																					<div class="form-group">
																						<label for="exampleInputPassword1">Password</label>
																						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="*** *** ***">
																					</div>
																					<button type="submit" class="btn btn-primary wd-login-btn">LOGIN</button>

																					<div class="form-check">
																						<label class="form-check-label">
																							<input type="checkbox" class="form-check-input">
																							Save this password
																						</label>
																					</div>
																					
																					<div class="wd-policy">
																						<p>
																							By Continuing. I conferm that i have read and userstand the <a href="#">terms of uses</a> and <a href="#">Privacy Policy</a>. 
																							Don’t have an account? <a href="#" class="black-color"><strong><u>Sign up</u></strong></a>
																						</p>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>

															</div>
															<div class="tab-pane fade" id="sign-up-2" role="tabpanel" aria-labelledby="sign-up-tab-2">

																<div class="row">
																	<div class="col-md-12 p0 brand-login-section">
																		<img src="{{URL::to('img/login-img-2.jpg')}}"class="img-fluid" alt="">
																		<div class="brand-description">
																			<div class="brand-logo">
																				<img src="{{URL::to('img/logo.png')}}"alt="Logo">
																			</div>
																			<div class="modal-description">
																				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod teoccaecvoluptatem.</p>
																			</div>
																			<ul class="list-unstyled">
																				<li class="media">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																				<li class="media my-4">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																				<li class="media">
																					<i class="fa fa-check" aria-hidden="true"></i>
																					<div class="media-body">
																						Lorem ipsum dolor sit amet, consecadipisicing 
																						elit, sed do eiusmod teoccaecvoluptatem.
																					</div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="col-12 col-md-12 col-lg-6 p0">
																		<div class="sign-up-section text-center">
																			<div class="login-form text-left">
																				<form>
																					<div class="form-group">
																						<label for="exampleInputname-login-in">First name</label>
																						<input type="text" class="form-control" id="exampleInputname-login-in" placeholder="First Name">
																					</div>
																					<div class="form-group">
																						<label for="exampleInputname-login-in-2">Last name</label>
																						<input type="text" class="form-control" id="exampleInputname-login-in-2" placeholder="Last Name">
																					</div>
																					<div class="form-group">
																						<label for="exampleInputEmail-login-in">Email</label>
																						<input type="text" class="form-control" id="exampleInputEmail-login-in" placeholder="Enter you email ...">
																					</div>
																					<div class="form-group">
																						<label for="exampleInputPassword-login-in">Password</label>
																						<input type="password" class="form-control" id="exampleInputPassword-login-in" placeholder="*** *** ***">
																					</div>
																					<button type="submit" class="btn btn-primary wd-login-btn">Sign Up</button>
																					
																					<div class="wd-policy">
																						<p>
																							By Continuing. I conferm that i have read and userstand the <a href="#">terms of uses</a> and <a href="#">Privacy Policy</a>. 
																							Don’t have an account? <a href="#" class="black-color"><strong><u>Sign up</u></strong></a>
																						</p>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>

															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
    	</div>
    </section>

    <!-- =========================
        Product Details Section
    ============================== -->
    <section class="product-details">
    	<div class="container">
    		<div class="row">
				<div class="col-12 p0">
					<div class="page-location">
						<ul>
							<li><a href="#">
								Home / Shop <span class="divider">/</span>
							</a></li>
							<li><a class="page-location-active" href="#">
								Vigo SP111-31N-P2GH Spin 1
								<span class="divider">/</span>
							</a></li>
						</ul>
					</div>
				</div>
	    		<div class="col-12 product-details-section">
				    <!-- ====================================
				        Product Details Gallery Section
				    ========================================= -->
					<div class="row">
						<div class="product-gallery col-12 col-md-12 col-lg-6">
						    <!-- ====================================
						        Single Product Gallery Section
						    ========================================= -->
						    <div class="row">
								<div class="col-md-12 product-slier-details">
								    <ul id="lightSlider">
								        <li data-thumb="img/product-img/product-description-1.jpg">
								            <img class="figure-img img-fluid" src="{{URL::to('img/product-img/product-description-1.jpg')}}"alt="product-img" />
								        </li>
								        <li data-thumb="img/product-img/product-description-1.jpg">
								            <img class="figure-img img-fluid" src="{{URL::to('img/product-img/product-description-1.jpg')}}"alt="product-img" />
								        </li>
								        <li data-thumb="img/product-img/product-description-1.jpg">
								            <img class="figure-img img-fluid" src="{{URL::to('img/product-img/product-description-1.jpg')}}"alt="product-img" />
								        </li>
								        <li data-thumb="img/product-img/product-description-1.jpg">
								            <img class="figure-img img-fluid" src="{{URL::to('img/product-img/product-description-1.jpg')}}"alt="product-img" />
								        </li>
								        <li data-thumb="img/product-img/product-description-1.jpg">
								            <img class="figure-img img-fluid" src="{{URL::to('img/product-img/product-description-1.jpg')}}"alt="product-img" />
								        </li>
								        <li data-thumb="img/product-img/product-description-1.jpg">
								            <img class="figure-img img-fluid" src="{{URL::to('img/product-img/product-description-1.jpg')}}"alt="product-img" />
								        </li>
								    </ul>
								</div>
							</div>
						</div>
						<div class="col-6 col-12 col-md-12 col-lg-6">
							<div class="product-details-gallery">
								<div class="list-group">
									<h4 class="list-group-item-heading product-title">
										Vigo SP111-31N-P2GH Spin 1
									</h4>
									<div class="media">
										<div class="media-left media-middle">
											<div class="rating">
												<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
												<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
												<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
												<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
												<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											</div>
										</div>
										<div class="media-body">
											<p>3.7/5 <span class="product-ratings-text"> -1747 Ratings</span></p>
										</div>
									</div>
								</div>
								<div class="list-group content-list">
									<p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> 100% Original product</p>
									<p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Manufacturer Warranty</p>
								</div>
							</div>
							<div class="product-store row">
								<div class="col-12 product-store-box">
									<div class="row">
										<div class="col-3 p0 store-border-img">
											<img src="{{URL::to('img/product-store/product-store-img1.jpg')}}"class="figure-img img-fluid" alt="Product Img">
										</div>
										<div class="col-5 store-border-price text-center">
											<div class="price">
												<p>$234</p>
											</div>
										</div>
										<div class="col-4 store-border-button">
											<a href="https://www.amazon.com/" target="_blank" class="btn btn-primary wd-shop-btn pull-right">
												Buy it now
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 product-store-box">
									<div class="row">
										<div class="col-3 p0 store-border-img">
											<img src="{{URL::to('img/product-store/product-store-img2.jpg')}}"class="figure-img img-fluid" alt="Product Img">
										</div>
										<div class="col-5 store-border-price text-center">
											<div class="price">
												<p>$535</p>
											</div>
										</div>
										<div class="col-4 store-border-button">
											<a href="https://www.aliexpress.com/" target="_blank" class="btn btn-primary wd-shop-btn pull-right red-bg">
												Buy it now
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 product-store-box">
									<div class="row">
										<div class="col-3 p0 store-border-img">
											<img src="{{URL::to('img/product-store/product-store-img3.jpg')}}"class="figure-img img-fluid" alt="Product Img">
										</div>
										<div class="col-5 store-border-price">
											<span class="badge badge-secondary wd-badge text-uppercase">Best</span>
											<div class="price text-center">
												<p>$198</p>
											</div>
										</div>
										<div class="col-4 store-border-button">
											<a href="https://www.infibeam.com/" target="_blank" class="btn btn-primary wd-shop-btn pull-right orange-bg">
												Buy it now
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 product-store-box">
									<div class="row">
										<div class="col-3 p0 store-border-img">
											<img src="{{URL::to('img/product-store/product-store-img4.jpg')}}"class="figure-img img-fluid" alt="Product Img">
										</div>
										<div class="col-5 store-border-price text-center">
											<div class="price">
												<p>$634</p>
											</div>
										</div>
										<div class="col-4 store-border-button">
											<a href="https://www.ebay.com/" target="_blank" class="btn btn-primary wd-shop-btn pull-right green-bg">
												Buy it now
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 product-store-box">
									<div class="row">
										<div class="col-3 p0 store-border-img">
											<img src="{{URL::to('img/product-store/product-store-img5.jpg')}}"class="figure-img img-fluid" alt="Product Img">
										</div>
										<div class="col-5 store-border-price text-center">
											<div class="price">
												<p>$234</p>
											</div>
										</div>
										<div class="col-4 store-border-button">
											<a href="https://www.elipkart.com/" target="_blank" class="btn btn-primary wd-shop-btn pull-right blue-bg">
												Buy it now
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
	    		</div>
    		</div>
			<div class="row">
				<div class="col-12">
				<div class="wd-tab-section">
					<div class="bd-example bd-example-tabs">
					  <ul class="nav nav-pills mb-3 wd-tab-menu" id="pills-tab" role="tablist">
					    <li class="nav-item col-6 col-md">
					      <a class="nav-link active" id="description-tab" data-toggle="pill" href="#description-section" role="tab" aria-controls="description-section" aria-expanded="true">Description</a>
					    </li>
					    <li class="nav-item col-6 col-md">
					      <a class="nav-link" id="full-specifiction-tab" data-toggle="pill" href="#full-specifiction" role="tab" aria-controls="full-specifiction" aria-expanded="false">Ful Specifiction</a>
					    </li>
					    <li class="nav-item col-6 col-md">
					      <a class="nav-link" id="reviews-tab" data-toggle="pill" href="#reviews" role="tab" aria-controls="reviews" aria-expanded="false">Reviews</a>
					    </li>
					    <li class="nav-item col-6 col-md">
					      <a class="nav-link" id="price-history-tab" data-toggle="pill" href="#price-history" role="tab" aria-controls="price-history" aria-expanded="false">Price History</a>
					    </li>
					  </ul>
						<div class="tab-content" id="pills-tabContent">
							<div class="tab-pane fade active show " id="description-section" role="tabpanel" aria-labelledby="description-tab" aria-expanded="true">
								<div class="product-tab-content">
									<h4 class="description-title">Vigo SP111-31N-P2GH Spin 1 Details</h4>

									<h6 class="description-subtitle">Battery and Power</h6>
									<p class="description-subcontent">Those looking forward to an excellent trimmer at an affordable price will say they have found their ultimate solution in Kamei KM-2013 Trimmer. This Electric Hair Clipper from Kemei features in brown color with an ergonomically crafted outfit. The recharge time for this device is 480 minutes with the usage time of 40 minutes. You can plug and charge the device from a standard 220V, 50Hz power source. The technology and design assure a great trimming experience.</p>

									<h6 class="description-subtitle">Quality of Body and Combs</h6>
									<p class="description-subcontent">High Hardness Alloy Steel Blade. The attachment includes stubble comb which can suffice various applications. The device also comes with Superior lift cut technology which offers smooth trimming with the cutting intervals of 1 mm, 1.5 mm, 2 mm, 3 mm, 4 mm, 5 mm, 6 mm, 7 mm, 8 mm. The number of combs is one, and the number of trim settings are </p>

									<h6 class="description-subtitle">Usability</h6>
									<p class="description-subcontent">There are no additional epilator settings. Brush cleaning is facilitated to clean after every use. The device can be used in cordless fashion for extra convenience. So you do not have to worry about moving the unit to the desired location. The handle grip material is made of plastic and there is a pop-up trimmer as well. The different technical specifications make it highly convenient to use.</p>

									<h6 class="description-subtitle">Additional features</h6>
									<p class="description-subcontent">The device features a LED display. here are no additional epilator settings. Brush cleaning is facilitated to clean after every use. The device can be used in cordless fashion for extra convenience. S The interesting aspects of the hair trimmer include a fantastic design that supports ease of use, <a class="highlights-text" href="#">good battery backup and a great blade that</a></p>

									<div class="row tab-gallery">
										<div class="col-6 col-md-3">
											<img class="figure-img img-fluid" src="{{URL::to('img/product-img/tab-img-1.jpg')}}"alt="features">
										</div>
										<div class="col-6 col-md-3">
											<img class="figure-img img-fluid" src="{{URL::to('img/product-img/tab-img-2.jpg')}}"alt="features">
										</div>
										<div class="col-6 col-md-3">
											<img class="figure-img img-fluid" src="{{URL::to('img/product-img/tab-img-3.jpg')}}"alt="features">
										</div>
										<div class="col-6 col-md-3">
											<img class="figure-img img-fluid" src="{{URL::to('img/product-img/tab-img-4.jpg')}}"alt="features">
										</div>
									</div>

									<h6 class="description-subtitle">Features</h6>
									<p class="description-subcontent">Redefine your workday with the Palm Treo Pro smartphone. Perfectly balanced, you can respond to business and personal email, stay on top of appointments and contacts, and use Wi-Fi or GPS when you’re out and about. Then watch a video on YouTube, catch up with news and sports on the web, or listen to a few songs. Balance your work and play the way you like it, with the Palm Treo Pro.</p>

									<div class="row">
										<div class="col-12 col-md-12 col-lg-6">
											<ul class="description-list">
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Windows Mobile® 6.1 Professional Edition</li>
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Qualcomm® MSM7201 400MHz Processor</li>
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> 320x320 transflective colour TFT touchscreen</li>
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> HSDPA/UMTS/EDGE/GPRS/GSM radio</li>
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Tri-band UMTS — 850MHz, 1900MHz, 2100MHz</li>
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Quad-band GSM — 850/900/1800/1900</li>
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> 802.11b/g with WPA, WPA2, and 801.1x authentication</li>
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Built-in GPS</li>
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Bluetooth Version: 2.0 + Enhanced Data Rate</li>
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> 256MB storage (100MB user available), 128MB RAM</li>
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> 2.0 megapixel camera, up to 8x digital zoom and video capture</li>
												<li><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Removable, rechargeable 1500mAh lithium-ion battery</li>
											</ul>
										</div>
										<div class="col-12 col-md-12 col-lg-6">
											<img class="figure-img img-fluid" src="{{URL::to('img/product-img/tab-img-5.jpg')}}"alt="Features">
										</div>
									</div>
								</div>

								<hr>

								<div class="row tab-video-area">
									<div class="col-12 col-md-12 col-lg-6">
										<img class="figure-img img-fluid" src="{{URL::to('img/product-img/tab-img-6.jpg')}}"alt="Features">
									</div>
									<div class="col-12 col-md-12 col-lg-6 video-info">
										<h6 class="video-info-title">Some prons and Cons</h6>

										<p><strong class="video-info-subtitle">Pros:</strong>really great keyboard, good trackpad, alcantara, stand-out design, USB-A port, great screen, great battery life, Windows Hello</p>
										<p><strong class="video-info-subtitle">Pros:</strong>really great keyboard, good trackpad, alcantara, stand-out design, USB-A port, great screen, great battery life, Windows Hello</p>


										<p class="video-info-content">The device features a LED display. The interesting aspects of the hair trimmer include a fantastic design that supports ease of use, good battery backup and a great blade that performs well and lasts for so long.</p>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="full-specifiction">
								<h6>Full Specifiction</h6>
								<p class="wd-opacity">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores id assumenda, ex ab voluptatem doloremque soluta magnam eum nihil iusto maiores! Libero nisi maior</p>

								<ul class="list-group wd-info-section">
									<li class="list-group-item d-flex justify-content-between align-items-center p0">
										<div class="col-12 col-md-6 info-section">
											<p>Brand Name : Asus</p>
										</div>
										<div class="col-12 col-md-5 info-section">
											<p>Release Date : 2018</p>
										</div>
										<div class="col-1"></div>
									</li>
									<li class="list-group-item d-flex justify-content-between align-items-center p0">
										<div class="col-12 col-md-6 info-section">
											<p>Google Play: Yes</p>
										</div>
										<div class="col-12 col-md-5 info-section">
											<p>Unlock Phones: Yes</p>
										</div>
										<div class="col-1"></div>
									</li>
									<li class="list-group-item d-flex justify-content-between align-items-center p0">
										<div class="col-12 col-md-6 info-section">
											<p>Talk Time: 4-6h</p>
										</div>
										<div class="col-12 col-md-5 info-section">
											<p>Battery Type: Not Detachable</p>
										</div>
										<div class="col-1"></div>
									</li>
									<li class="list-group-item d-flex justify-content-between align-items-center p0">
										<div class="col-12 col-md-6 info-section">
											<p>Size: 154.6x75.2x8.35mm</p>
										</div>
										<div class="col-12 col-md-5 info-section">
											<p>Display Resolution: 1920x1080</p>
										</div>
										<div class="col-1"></div>
									</li>
									<li class="list-group-item d-flex justify-content-between align-items-center p0">
										<div class="col-12 col-md-6 info-section">
											<p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
										</div>
										<div class="col-12 col-md-5 info-section">
											<p>Battery Capacity(mAh): 4000mAh</p>
										</div>
										<div class="col-1"></div>
									</li>
									<li class="list-group-item d-flex justify-content-between align-items-center p0">
										<div class="col-12 col-md-6 info-section">
											<p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
										</div>
										<div class="col-12 col-md-5 info-section">
											<p>CPU Manufacturer: Qualcomm</p>
										</div>
										<div class="col-1"></div>
									</li>
									<li class="list-group-item d-flex justify-content-between align-items-center p0">
										<div class="col-12 col-md-6 info-section">
											<p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
										</div>
										<div class="col-12 col-md-5 info-section">
											<p>CPU: Octa Core</p>
										</div>
										<div class="col-1"></div>

									</li>
									<li class="list-group-item d-flex justify-content-between align-items-center p0">
										<div class="col-12 col-md-6 info-section">
											<p>ROM: 16G</p>
										</div>
										<div class="col-12 col-md-5 info-section">
											<p>SlotsDesign: Bar</p>
										</div>
										<div class="col-1"></div>
									</li>
								</ul>
							</div>
							<div class="tab-pane fadereviews-section" id="reviews">
								<div class="row">
									<div class="col-12">
										<p class="wd-opacity">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores id assumenda, ex ab voluptatem doloremque soluta magnam eum nihil iusto maiores! Libero nisi maior</p>

										<h6 class="review-rating-title">Average Ratings and Reviews</h6>
										<div class="row tab-rating-bar-section">
											<div class="col-8 col-md-4 col-lg-4">
												<img src="{{URL::to('img/review-bg.png')}}"alt="review-bg">
												<div class="review-rating text-center">
													<h1 class="rating">4.5</h1>
													<p>4 Ratings &amp;
													0 Reviews</p>
												</div>
											</div>
											<div class="col-12 col-md-3 rating-bar-section">
												<div class="media rating-star-area">
													<p>5 <i class="fa fa-star" aria-hidden="true"></i></p>
													<div class="media-body rating-bar">
														<div class="progress wd-progress">
															<div class="progress-bar wd-bg-green" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
													</div>
												</div>
												<div class="media rating-star-area">
													<p>4 <i class="fa fa-star" aria-hidden="true"></i></p>
													<div class="media-body rating-bar">
														<div class="progress wd-progress">
															<div class="progress-bar wd-bg-green" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
													</div>
												</div>
												<div class="media rating-star-area">
													<p>3 <i class="fa fa-star" aria-hidden="true"></i></p>
													<div class="media-body rating-bar">
														<div class="progress wd-progress">
															<div class="progress-bar wd-bg-green" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
													</div>
												</div>
												<div class="media rating-star-area">
													<p>2 <i class="fa fa-star" aria-hidden="true"></i></p>
													<div class="media-body rating-bar">
														<div class="progress wd-progress">
															<div class="progress-bar wd-bg-yellow" role="progressbar" style="width: 35%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
													</div>
												</div>
												<div class="media rating-star-area">
													<p>1 <i class="fa fa-star" aria-hidden="true"></i></p>
													<div class="media-body rating-bar">
														<div class="progress wd-progress">
															<div class="progress-bar wd-bg-red" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
													</div>
												</div>
											</div>
										</div>

										<hr>
										
										<div class="reviews-market">
											<div class="reviews-title text-center">
												<h3>Ratings and Reviews From Market</h3>
												<hr>
											</div>
											<!-- 
												=================================
												Review Our Market
												=================================
											-->
											<div class="star-view-market">
												<div class="row">
													<div class="col-6 col-md-3 col-lg-2">
														<img src="{{URL::to('img/client/reviews-star-img1.png')}}"alt="client-img">
														<span class="badge badge-secondary wd-star-market-badge text-uppercase">4.5</span>
														<div class="rating-market-section">
															<div class="rating-star">
																<div class="review-rating-yellow-5"></div><span class="rating-number">5</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-yellow-4"></div><span class="rating-number">4</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-yellow-3"></div><span class="rating-number">3</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-yellow-2"></div><span class="rating-number">2</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-yellow-1"></div><span class="rating-number">2</span>
															</div>
														</div>
													</div>
													<div class="col-6 col-md-3 col-lg-2">
														<img src="{{URL::to('img/client/reviews-star-img1.png')}}"alt="client-img">
														<span class="badge badge-secondary wd-star-market-badge text-uppercase">5.0</span>
														<div class="rating-market-section">
															<div class="rating-star">
																<div class="review-rating-blue-5"></div><span class="rating-number">5</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-blue-4"></div><span class="rating-number">4</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-blue-3"></div><span class="rating-number">2</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-blue-2"></div><span class="rating-number">3</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-blue-1"></div><span class="rating-number">4</span>
															</div>
														</div>
													</div>
													<div class="col-6 col-md-3 col-lg-2">
														<img src="{{URL::to('img/client/reviews-star-img1.png')}}"alt="client-img">
														<span class="badge badge-secondary wd-star-market-badge text-uppercase">4.5</span>
														<div class="rating-market-section">
															<div class="rating-star">
																<div class="review-rating-red-5"></div><span class="rating-number">5</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-red-4"></div><span class="rating-number">2</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-red-3"></div><span class="rating-number">3</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-red-2"></div><span class="rating-number">4</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-red-1"></div><span class="rating-number">5</span>
															</div>
														</div>
													</div>
													<div class="col-6 col-md-3 col-lg-2">
														<img src="{{URL::to('img/client/reviews-star-img1.png')}}"alt="client-img">
														<span class="badge badge-secondary wd-star-market-badge text-uppercase">4.5</span>
														<div class="rating-market-section">
															<div class="rating-star">
																<div class="review-rating-green-5"></div><span class="rating-number">5</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-green-4"></div><span class="rating-number">1</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-green-3"></div><span class="rating-number">5</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-green-2"></div><span class="rating-number">3</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-green-1"></div><span class="rating-number">2</span>
															</div>
														</div>
													</div>
													<div class="col-6 col-md-3 col-lg-2">
														<img src="{{URL::to('img/client/reviews-star-img1.png')}}"alt="client-img">
														<span class="badge badge-secondary wd-star-market-badge text-uppercase">4.5</span>
														<div class="rating-market-section">
															<div class="rating-star">
																<div class="review-rating-dark-yellow-5"></div><span class="rating-number">5</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-dark-yellow-4"></div><span class="rating-number">4</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-dark-yellow-3"></div><span class="rating-number">3</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-dark-yellow-2"></div><span class="rating-number">2</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-dark-yellow-1"></div><span class="rating-number">3</span>
															</div>
														</div>
													</div>
													<div class="col-6 col-md-3 col-lg-2">
														<img src="{{URL::to('img/client/reviews-star-img1.png')}}"alt="client-img">
														<span class="badge badge-secondary wd-star-market-badge text-uppercase">4.5</span>
														<div class="rating-market-section">
															<div class="rating-star">
																<div class="review-rating-light-yellow-5"></div><span class="rating-number">5</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-light-yellow-4"></div><span class="rating-number">4</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-light-yellow-3"></div><span class="rating-number">3</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-light-yellow-2"></div><span class="rating-number">2</span>
															</div>
															<div class="rating-star">
																<div class="review-rating-light-yellow-1"></div><span class="rating-number">3</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<hr>
											<!-- 
												=================================
												Review Our Product
												=================================
											-->
											<div class="review-our-product text-left row">
												<div class="col-12 col-lg-6 reviews-title">
													<h3>Review to our Blurb</h3>
												</div>

												<div class="col-12 col-lg-6 text-right display-none-md">
													<div class="filter">
														<div class="btn-group" role="group">
															<div class="d-flex">
															 	<p>View as:</p>
															    <button id="btnGroupDropwdreview" type="button" class="btn btn-secondary dropdown-toggle filter-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															      New
															    </button>
															    <div class="dropdown-menu" aria-labelledby="btnGroupDropwdreview" style="position: absolute; transform: translate3d(50px, 29px, 0px); top: 0px; left: 0px; will-change: transform;">
															      <a class="dropdown-item" href="#">Camara</a>
															      <a class="dropdown-item" href="#">Joystick</a>
															    </div>
															</div>
														</div>
														<button type="button" class="btn btn-primary btn-sm add-review-btn">Add your review</button>
													</div>
												</div>
												 
												<!-- =================================
													Review Client Section
													================================= -->
												
												<div class="col-12 review-our-product-area">
													<div class="row">
														<div class="col-12 col-md-6">
															<div class="row">
																<div class="col-12">
																	<div class="media">
																	  <div class="media-left media-middle">
																	    <a href="#">
																	      <img class="media-object" src="{{URL::to('img/client/client-img-1.png')}}"alt="client-img">
																	    </a>
																	  </div>
																	  <div class="media-body">
																	    <h4 class="media-heading client-title">Robert Strud</h4>
																		<div class="client-subtitle">Affiliate at <a href="#">Market 1</a></div>
																	  </div>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-12 col-md-6 review-date-time">
															<p class="review-date">November 17, 2016</p>
															<p class="review-time">at 11:52 pm</p>
														</div>
													</div>
													<div class="row">
														<div class="col-12"></div>
														<div class="col-6 col-md-4">
															<div class="rating-market-section">
																<span class="badge badge-secondary wd-star-market-badge text-uppercase">4.5 <i class="fa fa-star-o" aria-hidden="true"></i></span>
																<div class="rating-star">
																	<div class="review-rating-light-yellow-5"></div><span class="rating-number">5</span>
																</div>
																<div class="rating-star">
																	<div class="review-rating-light-yellow-4"></div><span class="rating-number">4</span>
																</div>
																<div class="rating-star">
																	<div class="review-rating-light-yellow-3"></div><span class="rating-number">3</span>
																</div>
																<div class="rating-star">
																	<div class="review-rating-light-yellow-2"></div><span class="rating-number">2</span>
																</div>
																<div class="rating-star">
																	<div class="review-rating-light-yellow-1"></div><span class="rating-number">3</span>
																</div>
															</div>
														</div>
														<div class="col-6 col-md-4">
															<div class="client-review-list">
																<div class="media">
																  <div class="media-left media-middle">
																    <a href="#">
																      <img class="media-object" src="{{URL::to('img/client/client-list-icon-1.png')}}"alt="client-img">
																    </a>
																  </div>
																  <div class="media-body">
																    <h6 class="media-heading">Prons</h6>
																  </div>
																</div>
																<ul class="check-list">
																	<li><i class="fa fa-check" aria-hidden="true"></i> All</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Design</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Developing</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Metalic</li>
																</ul>
															</div>
														</div>
														<div class="col-6 col-md-4">
															<div class="client-review-list">
																<div class="media">
																  <div class="media-left media-middle">
																    <a href="#">
																      <img class="media-object" src="{{URL::to('img/client/client-list-icon-2.png')}}"alt="client-img">
																    </a>
																  </div>
																  <div class="media-body">
																    <h6 class="media-heading">Prons</h6>
																  </div>
																</div>
																<ul class="check-list icon-red">
																	<li><i class="fa fa-check" aria-hidden="true"></i> All</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Design</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Developing</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Metalic</li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<div class="col-12 review-our-product-area">
													<div class="row">
														<div class="col-12 col-md-6">
															<div class="row">
																<div class="col-12">
																	<div class="media">
																	  <div class="media-left media-middle">
																	    <a href="#">
																	      <img class="media-object" src="{{URL::to('img/client/client-img-1.png')}}"alt="client-img">
																	    </a>
																	  </div>
																	  <div class="media-body">
																	    <h4 class="media-heading client-title">Faisal Kanon</h4>
																		<div class="client-subtitle">Affiliate at <a href="#">Market 2</a></div>
																	  </div>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-12 col-md-6 review-date-time">
															<p class="review-date">November 17, 2016</p>
															<p class="review-time">at 11:52 pm</p>
														</div>
													</div>
													<div class="row">
														<div class="col-12"></div>
														<div class="col-6 col-md-4">
															<div class="rating-market-section">
																<span class="badge badge-secondary wd-star-market-badge text-uppercase">4.5 <i class="fa fa-star-o" aria-hidden="true"></i></span>
																<div class="rating-star">
																	<div class="review-rating-light-yellow-5"></div><span class="rating-number">5</span>
																</div>
																<div class="rating-star">
																	<div class="review-rating-light-yellow-4"></div><span class="rating-number">4</span>
																</div>
																<div class="rating-star">
																	<div class="review-rating-light-yellow-3"></div><span class="rating-number">3</span>
																</div>
																<div class="rating-star">
																	<div class="review-rating-light-yellow-2"></div><span class="rating-number">2</span>
																</div>
																<div class="rating-star">
																	<div class="review-rating-light-yellow-1"></div><span class="rating-number">3</span>
																</div>
															</div>
														</div>
														<div class="col-6 col-md-4">
															<div class="client-review-list">
																<div class="media">
																  <div class="media-left media-middle">
																    <a href="#">
																      <img class="media-object" src="{{URL::to('img/client/client-list-icon-1.png')}}"alt="client-img">
																    </a>
																  </div>
																  <div class="media-body">
																    <h6 class="media-heading">Prons</h6>
																  </div>
																</div>
																<ul class="check-list">
																	<li><i class="fa fa-check" aria-hidden="true"></i> All</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Design</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Developing</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Metalic</li>
																</ul>
															</div>
														</div>
														<div class="col-6 col-md-4">
															<div class="client-review-list">
																<div class="media">
																  <div class="media-left media-middle">
																    <a href="#">
																      <img class="media-object" src="{{URL::to('img/client/client-list-icon-2.png')}}"alt="client-img">
																    </a>
																  </div>
																  <div class="media-body">
																    <h6 class="media-heading">Prons</h6>
																  </div>
																</div>
																<ul class="check-list icon-red">
																	<li><i class="fa fa-check" aria-hidden="true"></i> All</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Design</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Developing</li>
																	<li><i class="fa fa-check" aria-hidden="true"></i> Metalic</li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>

											<!-- 
												=================================
												Review Comment Section
												=================================
											-->
											<div class="review-comment-section">
												<div class="row">
													<div class="col-12 col-md-12 col-lg-12 col-xl-8">
														<div class="reviews-title leave-opinion">
															<h3>Leave your Opinion here</h3>
														</div>
														<form>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label for="first" class="color-black">Name *</label>
																		<input type="text" class="form-control" placeholder="" id="first">
																	</div>
																</div>

																<div class="col-md-6">
																	<div class="form-group">
																		<label for="last" class="color-black">Email *</label>
																		<input type="email" class="form-control" placeholder="" id="last">
																	</div>
																</div>

																<div class="col-md-6">
																	<div class="form-group">
																		<label for="last" class="color-green">Prons</label>
																			<textarea class="form-control col-md-12" id="exampleFormControlTextarea1" placeholder="Write your Opinion here ... "></textarea>
																	</div>
																</div>

																<div class="col-md-6">
																	<div class="form-group">
																		<label for="exampleFormControlTextarea2" class="color-red">Cons</label>
																		<textarea class="form-control col-12" id="exampleFormControlTextarea2" placeholder="Write your Opinion here ... "></textarea>
																	</div>
																</div>

																<div class="col-12 col-md-12 product-rating-area">
																	<div class="product-rating-ph">
																		<div class="rating-area">
																			<div class="d-flex justify-content-between">
																				<p>Camera</p>
																				<div class="rating">
																					<a href="#"><i class="fa fa-star cat-1" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-2" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-3" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-4" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-5" aria-hidden="true"></i></a>
																				</div>
																			</div>
																			<div class="rating-slider-1"></div>
																		</div>
																		<div class="rating-area">
																			<div class="d-flex justify-content-between">
																				<p>Video Quality</p>
																				<div class="rating">
																					<a href="#"><i class="fa fa-star cat-2-1" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-2-2" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-2-3" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-2-4" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-2-5" aria-hidden="true"></i></a>
																				</div>
																			</div>
																			<div class="rating-slider-2"></div>
																		</div>
																		<div class="rating-area">
																			<div class="d-flex justify-content-between">
																				<p>Box Quality</p>
																				<div class="rating">
																					<a href="#"><i class="fa fa-star cat-3-1" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-3-2" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-3-3" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-3-4" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-3-5" aria-hidden="true"></i></a>
																				</div>
																			</div>
																			<div class="rating-slider-3"></div>
																		</div>
																		<div class="rating-area">
																			<div class="d-flex justify-content-between">
																				<p>Video Quality</p>
																				<div class="rating">
																					<a href="#"><i class="fa fa-star cat-4-1" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-4-2" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-4-3" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-4-4" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-4-5" aria-hidden="true"></i></a>
																				</div>
																			</div>
																			<div class="rating-slider-4"></div>
																		</div>
																		<div class="rating-area">
																			<div class="d-flex justify-content-between">
																				<p>Box Quality</p>
																				<div class="rating">
																					<a href="#"><i class="fa fa-star cat-5-1" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-5-2" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-5-3" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-5-4" aria-hidden="true"></i></a>
																					<a href="#"><i class="fa fa-star cat-5-5" aria-hidden="true"></i></a>
																				</div>
																			</div>
																			<div class="rating-slider-5"></div>
																		</div>
																	</div>
																</div>

																<div class="col-md-12">
																	<button type="submit" class="btn btn-primary review-comment"><i class="fa fa-check" aria-hidden="true"></i> Post Comment</button>
																</div>
															</div>
														</form>
													</div>

													<div class="col-12 col-md-12 col-lg-12 col-xl-4 product-rating-area">
														<div class="product-rating-list product-rating-desktop">
															<div class="rating-area">
																<div class="d-flex justify-content-between">
																	<p>Camera</p>
																	<div class="rating">
																		<a href="#"><i class="fa fa-star cat-1" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-2" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-3" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-4" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-5" aria-hidden="true"></i></a>
																	</div>
																</div>
																<div class="rating-slider-1"></div>
															</div>
															<div class="rating-area">
																<div class="d-flex justify-content-between">
																	<p>Video Quality</p>
																	<div class="rating">
																		<a href="#"><i class="fa fa-star cat-2-1" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-2-2" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-2-3" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-2-4" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-2-5" aria-hidden="true"></i></a>
																	</div>
																</div>
																<div class="rating-slider-2"></div>
															</div>
															<div class="rating-area">
																<div class="d-flex justify-content-between">
																	<p>Box Quality</p>
																	<div class="rating">
																		<a href="#"><i class="fa fa-star cat-3-1" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-3-2" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-3-3" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-3-4" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-3-5" aria-hidden="true"></i></a>
																	</div>
																</div>
																<div class="rating-slider-3"></div>
															</div>
															<div class="rating-area">
																<div class="d-flex justify-content-between">
																	<p>Video Quality</p>
																	<div class="rating">
																		<a href="#"><i class="fa fa-star cat-4-1" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-4-2" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-4-3" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-4-4" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-4-5" aria-hidden="true"></i></a>
																	</div>
																</div>
																<div class="rating-slider-4"></div>
															</div>
															<div class="rating-area">
																<div class="d-flex justify-content-between">
																	<p>Box Quality</p>
																	<div class="rating">
																		<a href="#"><i class="fa fa-star cat-5-1" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-5-2" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-5-3" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-5-4" aria-hidden="true"></i></a>
																		<a href="#"><i class="fa fa-star cat-5-5" aria-hidden="true"></i></a>
																	</div>
																</div>
																<div class="rating-slider-5"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade specifiction-section" id="price-history">
								<div class="row">
								  <div class="col-12 col-md-5">
								  	<h2 class="specifiction-title">Specifiction</h2>
									<ul class="list-group specifiction-list">
									  <li class="list-group-item">Brand Name : <span>Asus</span></li>
									  <li class="list-group-item">Google Play: <span>Yes</span></li>
									  <li class="list-group-item">Talk Time: <span>4-6h</span></li>
									  <li class="list-group-item">Size: <span>154.6x75.2x8.35mm</span></li>
									  <li class="list-group-item">Feature: <span>Gravity Response,MP3 Playback,Touchscreen,GPS</span></li>
									  <li class="list-group-item">CPU: <span>Octa Core</span></li>
									  <li class="list-group-item">ROM: <span>16G</span></li>
									  <li class="list-group-item">Release Date : <span>2018</span></li>
									  <li class="list-group-item">Unlock Phones: <span>Yes</span></li>
									  <li class="list-group-item">Battery Type: <span>Not Detachable</span></li>
									  <li class="list-group-item">Display Resolution: <span>1920x1080</span></li>
									  <li class="list-group-item">Battery Capacity(mAh): <span>4000mAh</span></li>
									  <li class="list-group-item">CPU Manufacturer: <span>Qualcomm</span></li>
									  <li class="list-group-item">SlotsDesign: <span>Bar</span></li>
									</ul>
								  </div>
			
								  <div class="col-12 related-articles">
								
									<div class="row">
						
								  </div>
							  	</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>

    	</div>
    </section>






    <!-- =========================
        Footer Section
    ============================== -->
    <footer class="footer wow fadeInUp animated" data-wow-delay="900ms">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- ===========================
                            Footer About
                         =========================== -->
                    <div class="footer-about">
                        <a href="#" class="footer-about-logo">
                       	<img  style="margin-top: -70px !important;" src="{{URL::to('assets/images/wikicomparelogoblue.png')}}" alt="Logo">
                        </a>
                        <div class="footer-description">
                            <p>Lorem ipsum dolor sit amet, anim id est laborum. Sed ut perspconsectetur, adipisci vam aliquam qua.</p>
                        </div>
                        <div class="wb-social-media">
						<a href="#" class="bh"><i class="fa fa-behance"></i></a>
						<a href="#" class="fb"><i class="fa fa-facebook-official"></i></a>
						<a href="#" class="db"><i class="fa fa-dribbble"></i></a>
						<a href="#" class="gp"><i class="fa fa-google-plus"></i></a>
						<a href="#" class="vn"><i class="fa fa-vine"></i></a>
						<a href="#" class="yt"><i class="fa fa-youtube-play"></i></a>
					</div>
                    </div>
                </div>
                <div class="col-md-2 footer-view-controller">
                    <!-- ===========================
                            Festival Deals
                         =========================== -->
                    <div class="footer-nav">
                        <h6 class="footer-subtitle active-color">Footer Menu</h6>
                        <ul>
                            <li><a href="index.html"> Home </a></li>
                      
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 footer-view-controller">
                    <!-- ===========================
                            Top Stores
                         =========================== -->
                    <div class="footer-nav">
                        <div class="stores-list">
                            <h6 class="footer-subtitle">Top Stores</h6>
                            <ul>
                                <li><a href="shop-left-sidebar.html">LINK</a></li>
                            
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 footer-view-controller">
                    <!-- ===========================
                            Need Help ?
                         =========================== -->
                    <div class="footer-nav">
                        <h6 class="footer-subtitle">Need Help ?</h6>
                        <ul>
                           <li><a href="shop-left-sidebar.html">LINK</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 footer-view-controller">
                    <!-- ===========================
                            About
                         =========================== -->
                    <div class="footer-nav">
                        <h6 class="footer-subtitle">About</h6>
                        <ul>
                           <li><a href="shop-left-sidebar.html">LINK</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- =========================
        CopyRight
    ============================== -->
    <section class="copyright wow fadeInUp animated" data-wow-delay="1500ms">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copyright-text">
                        <p class="text-uppercase">COPYRIGHT &copy; 2021 WIKICOMPARE</p>
                    </div>
                </div>
      
            </div>
        </div>
    </section>

    <!-- =========================
    	Main Loding JS Script
    ============================== -->
    <script src="{{URL::to('js/jquery.min.js')}}"></script>
    <script src="{{URL::to('js/jquery-ui.js')}}"></script>
    <script src="{{URL::to('js/popper.js')}}"></script>
    <script src="{{URL::to('js/bootstrap.min.js')}}"></script>
    <script src="{{URL::to('js/jquery.counterup.min.js')}}"></script>
    <script src="{{URL::to('js/jquery.nav.js')}}"></script>
    <!-- <script src="{{URL::to('js/jquery.nicescroll.js')}}"></script> -->
    <script src="{{URL::to('js/jquery.rateyo.js')}}"></script>
    <script src="{{URL::to('js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{URL::to('js/jquery.sticky.js')}}"></script>
    <script src="{{URL::to('js/mobile.js')}}"></script>
    <script src="{{URL::to('js/lightslider.min.js')}}"></script>
    <script src="{{URL::to('js/owl.carousel.min.js')}}"></script>
    <script src="{{URL::to('js/circle-progress.min.js')}}"></script>
    <script src="{{URL::to('js/waypoints.min.js')}}"></script>

    <script src="{{URL::to('js/simplePlayer.js')}}"></script>
    
    <script src="{{URL::to('js/main.js')}}"></script>
  </body>

<!-- Mirrored from www.themeim.com/demo/blurb/demo/product-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Jan 2021 21:44:07 GMT -->
</html>