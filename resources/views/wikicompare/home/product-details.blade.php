@extends('layouts.frontend_default')
@section('content')
    <section class="product-details">
    	<div class="container">
            <div class="row compare-folding-section">
                <div class="col-12 text-center">
                    <h2 class="compare-folding-title">Compare your Product</h2>
                    <div class="page-location pt-0">
                        <ul>
                            <li><a href="#">
                                Home <span class="divider">/</span>
                            </a></li>
                            <li><a href="#">
                                Shop <span class="divider">/</span>
                            </a></li>
                            <li><a class="page-location-active" href="#">
                                Proudct Compare
                                <span class="divider">/</span>
                            </a></li>
                        </ul>
                    </div>
                </div>
            </div>
			<div class="row compare-products">
				<div class="col-12 p0">
                    <table class="col-md-12 p0 table table-responsive">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="choose-market text-center">
										<div class="compare-product-add">
											<i class="fa fa-hand-o-right" aria-hidden="true"></i>
											<h6 class="compare-product-tile">Compare product</h6>
											<p><u>5 products added</u></p>
										</div>
                                    </div>

                                </td>
                                <td class="compare-details-section">
                                    <img src="{{URL::to('img/compare/compare-img-1.jpg')}}" class="figure-img img-fluid" alt="compare">
                                    <div class="product-detail-ph">
                                        <button type="button" class="btn btn-info product-detail-collapse" data-toggle="collapse" data-target="#product-detail-1">Product Details <i class="fa fa-bars float-right" aria-hidden="true"></i></button>
                                        <div id="product-detail-1" class="collapse">
                                            <div class="compare-price text-center">
                                                <h6 class="compare-details-title">Visual D300S</h6>
                                                <ul class="full-specifiction">
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Brand Name : Asus</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Release Date : 2018</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Release Date : 2018</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Google Play: Yes</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Unlock Phones: Yes</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Talk Time: 4-6h</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Battery Type: Not Detachable</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Size: 154.6x75.2x8.35mm</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Display Resolution: 1920x1080</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Battery Capacity(mAh): 4000mAh</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>CPU Manufacturer: Qualcomm</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>CPU: Octa Core</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>ROM: 16G</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>SlotsDesign: Bar</p>
                                                        </div>
                                                    </li>
                                                </ul>
												<div class="compare-details-price">
													<a href="https://www.amazon.com/" target="_blank" class="btn btn-primary btn-sm compare-details-btn">
														<p class="pull-left">Buy now</p>
														<img src="{{URL::to('img/compare/btn-img-1.png')}}" class="figure-img img-fluid" alt="compare">
														<p class="pull-right">$299</p>
													</a>
													<img src="{{URL::to('img/compare/best-price.png')}}" class="figure-img img-fluid" alt="compare">
												</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="compare-price text-center product-detail-desktop">
                                        <h6 class="compare-details-title">Visual D300S</h6>
                                        <ul class="full-specifiction">
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Brand Name : Asus</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Release Date : 2018</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Release Date : 2018</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Google Play: Yes</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Unlock Phones: Yes</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Talk Time: 4-6h</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Battery Type: Not Detachable</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Size: 154.6x75.2x8.35mm</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Display Resolution: 1920x1080</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Battery Capacity(mAh): 4000mAh</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>CPU Manufacturer: Qualcomm</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>CPU: Octa Core</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>ROM: 16G</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>SlotsDesign: Bar</p>
                                                </div>
                                            </li>
                                        </ul>
										<div class="compare-details-price">
											<a href="https://www.amazon.com/" target="_blank" class="btn btn-primary btn-sm compare-details-btn">
                                                <div class="d-flex justify-content-between">
    												<p class="pull-left">Buy now</p>
    												<img src="{{URL::to('img/compare/btn-img-1.png')}}" class="figure-img img-fluid" alt="compare">
    												<p class="pull-right">$299</p>
                                                </div>
											</a>
											<img src="{{URL::to('img/compare/best-price.png')}}" class="figure-img img-fluid" alt="compare">
										</div>
                                    </div>
                                </td>
                                <td class="compare-details-section">
                                    <img src="{{URL::to('img/compare/compare-img-2.jpg')}}" class="figure-img img-fluid" alt="compare">
                                    <div class="product-detail-ph">
                                        <button type="button" class="btn btn-info product-detail-collapse" data-toggle="collapse" data-target="#product-detail-2">Product Details <i class="fa fa-bars float-right" aria-hidden="true"></i></button>
                                        <div id="product-detail-2" class="collapse">
                                            <div class="compare-price text-center">
                                                <h6 class="compare-details-title">Visual D300S</h6>
                                                <ul class="full-specifiction">
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Brand Name : Asus</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Release Date : 2018</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Release Date : 2018</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Google Play: Yes</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Unlock Phones: Yes</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Talk Time: 4-6h</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Battery Type: Not Detachable</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Size: 154.6x75.2x8.35mm</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Display Resolution: 1920x1080</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Battery Capacity(mAh): 4000mAh</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>CPU Manufacturer: Qualcomm</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>CPU: Octa Core</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>ROM: 16G</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>SlotsDesign: Bar</p>
                                                        </div>
                                                    </li>
                                                </ul>
												<div class="compare-details-price">
													<a href="https://www.ebay.com/" target="_blank" class="btn btn-primary btn-sm compare-details-btn">
														<p class="pull-left">Buy now</p>
														<img src="{{URL::to('img/compare/btn-img-2.png')}}" class="figure-img img-fluid" alt="compare">
														<p class="pull-right">$299</p>
													</a>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="compare-price text-center product-detail-desktop">
                                        <h6 class="compare-details-title">Visual D300S</h6>
                                        <ul class="full-specifiction">
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Brand Name : Asus</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Release Date : 2018</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Release Date : 2018</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Google Play: Yes</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Unlock Phones: Yes</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Talk Time: 4-6h</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Battery Type: Not Detachable</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Size: 154.6x75.2x8.35mm</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Display Resolution: 1920x1080</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Battery Capacity(mAh): 4000mAh</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>CPU Manufacturer: Qualcomm</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>CPU: Octa Core</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>ROM: 16G</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>SlotsDesign: Bar</p>
                                                </div>
                                            </li>
                                        </ul>
										<div class="compare-details-price">
											<a href="https://www.ebay.com/" target="_blank" class="btn btn-primary btn-sm compare-details-btn">
                                                <div class="d-flex justify-content-between">
    												<p class="pull-left">Buy now</p>
    												<img src="{{URL::to('img/compare/btn-img-2.png')}}" class="figure-img img-fluid" alt="compare">
    												<p class="pull-right">$299</p>
                                                </div>
											</a>
										</div>
                                    </div>
                                </td>
                                <td class="compare-details-section">
                                    <img src="{{URL::to('img/compare/compare-img-3.jpg')}}" class="figure-img img-fluid" alt="compare">
                                    <div class="product-detail-ph">
                                        <button type="button" class="btn btn-info product-detail-collapse" data-toggle="collapse" data-target="#product-detail-3">Product Details <i class="fa fa-bars float-right" aria-hidden="true"></i></button>
                                        <div id="product-detail-3" class="collapse">
                                            <div class="compare-price text-center">
                                                <h6 class="compare-details-title">Visual D300S</h6>
                                                <ul class="full-specifiction">
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Brand Name : Asus</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Release Date : 2018</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Release Date : 2018</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Google Play: Yes</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Unlock Phones: Yes</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Talk Time: 4-6h</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Battery Type: Not Detachable</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Size: 154.6x75.2x8.35mm</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Display Resolution: 1920x1080</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Battery Capacity(mAh): 4000mAh</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>CPU Manufacturer: Qualcomm</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>CPU: Octa Core</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>ROM: 16G</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>SlotsDesign: Bar</p>
                                                        </div>
                                                    </li>
                                                </ul>
										<div class="compare-details-price">
											<a href="https://www.elipkart.com/" target="_blank" class="btn btn-primary btn-sm compare-details-btn">
												<p class="pull-left">Buy now</p>
												<img src="{{URL::to('img/compare/btn-img-3.png')}}" class="figure-img img-fluid" alt="compare">
												<p class="pull-right">$299</p>
											</a>
										</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="compare-price text-center product-detail-desktop">
                                        <h6 class="compare-details-title">Visual D300S</h6>
                                        <ul class="full-specifiction">
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Brand Name : Asus</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Release Date : 2018</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Release Date : 2018</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Google Play: Yes</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Unlock Phones: Yes</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Talk Time: 4-6h</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Battery Type: Not Detachable</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Size: 154.6x75.2x8.35mm</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Display Resolution: 1920x1080</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Battery Capacity(mAh): 4000mAh</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>CPU Manufacturer: Qualcomm</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>CPU: Octa Core</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>ROM: 16G</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>SlotsDesign: Bar</p>
                                                </div>
                                            </li>
                                        </ul>
										<div class="compare-details-price">
											<a href="https://www.elipkart.com/" target="_blank" class="btn btn-primary btn-sm compare-details-btn">
                                                <div class="d-flex justify-content-between">
    												<p class="pull-left">Buy now</p>
    												<img src="{{URL::to('img/compare/btn-img-3.png')}}" class="figure-img img-fluid" alt="compare">
    												<p class="pull-right">$299</p>
                                                </div>
											</a>
										</div>
                                    </div>
                                </td>
                                <td class="compare-details-section">
                                    <img src="{{URL::to('img/compare/compare-img-4.jpg')}}" class="figure-img img-fluid" alt="compare">
                                    <div class="product-detail-ph">
                                        <button type="button" class="btn btn-info product-detail-collapse" data-toggle="collapse" data-target="#product-detail-4">Product Details <i class="fa fa-bars float-right" aria-hidden="true"></i></button>
                                        <div id="product-detail-4" class="collapse">
                                            <div class="compare-price text-center">
                                                <h6 class="compare-details-title">Visual D300S</h6>
                                                <ul class="full-specifiction">
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Brand Name : Asus</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Release Date : 2018</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Release Date : 2018</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Google Play: Yes</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Unlock Phones: Yes</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Talk Time: 4-6h</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Battery Type: Not Detachable</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Size: 154.6x75.2x8.35mm</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Display Resolution: 1920x1080</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Battery Capacity(mAh): 4000mAh</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>CPU Manufacturer: Qualcomm</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>CPU: Octa Core</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>ROM: 16G</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>SlotsDesign: Bar</p>
                                                        </div>
                                                    </li>
                                                </ul>
										<div class="compare-details-price">
											<a href="https://www.aliexpress.com/" target="_blank" class="btn btn-primary btn-sm compare-details-btn">
												<p class="pull-left">Buy now</p>
												<img src="{{URL::to('img/compare/btn-img-4.png')}}" class="figure-img img-fluid" alt="compare">
												<p class="pull-right">$299</p>
											</a>
										</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="compare-price text-center product-detail-desktop">
                                        <h6 class="compare-details-title">Visual D300S</h6>
                                        <ul class="full-specifiction">
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Brand Name : Asus</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Release Date : 2018</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Release Date : 2018</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Google Play: Yes</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Unlock Phones: Yes</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Talk Time: 4-6h</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Battery Type: Not Detachable</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Size: 154.6x75.2x8.35mm</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Display Resolution: 1920x1080</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Battery Capacity(mAh): 4000mAh</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>CPU Manufacturer: Qualcomm</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>CPU: Octa Core</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>ROM: 16G</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>SlotsDesign: Bar</p>
                                                </div>
                                            </li>
                                        </ul>
										<div class="compare-details-price">
											<a href="https://www.aliexpress.com/" target="_blank" class="btn btn-primary btn-sm compare-details-btn">
                                                <div class="d-flex justify-content-between">
    												<p class="pull-left">Buy now</p>
    												<img src="{{URL::to('img/compare/btn-img-4.png')}}" class="figure-img img-fluid" alt="compare">
    												<p class="pull-right">$299</p>
                                                </div>
											</a>
										</div>
                                    </div>
                                </td>
                                <td class="compare-details-section">
                                    <img src="{{URL::to('img/compare/compare-img-5.jpg')}}" class="figure-img img-fluid" alt="compare">
                                    <div class="product-detail-ph">
                                        <button type="button" class="btn btn-info product-detail-collapse" data-toggle="collapse" data-target="#product-detail-5">Product Details <i class="fa fa-bars float-right" aria-hidden="true"></i></button>
                                        <div id="product-detail-5" class="collapse">
                                            <div class="compare-price text-center">
                                                <h6 class="compare-details-title">Visual D300S</h6>
                                                <ul class="full-specifiction">
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Brand Name : Asus</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Release Date : 2018</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Release Date : 2018</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Google Play: Yes</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Unlock Phones: Yes</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Talk Time: 4-6h</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Battery Type: Not Detachable</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Size: 154.6x75.2x8.35mm</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Display Resolution: 1920x1080</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Battery Capacity(mAh): 4000mAh</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>CPU Manufacturer: Qualcomm</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-12">
                                                            <p>CPU: Octa Core</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>ROM: 16G</p>
                                                        </div>
                                                    </li>
                                                    <li class="row">
                                                        <div class="col-6">
                                                            <p>SlotsDesign: Bar</p>
                                                        </div>
                                                    </li>
                                                </ul>
												<div class="compare-details-price">
													<a href="https://www.snapdeal.com/" target="_blank" class="btn btn-primary btn-sm compare-details-btn">
														<p class="pull-left">Buy now</p>
														<img src="{{URL::to('img/compare/btn-img-5.png')}}" class="figure-img img-fluid" alt="compare">
														<p class="pull-right">$299</p>
													</a>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="compare-price text-center product-detail-desktop">
                                        <h6 class="compare-details-title">Visual D300S</h6>
                                        <ul class="full-specifiction">
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Brand Name : Asus</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Release Date : 2018</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Release Date : 2018</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Google Play: Yes</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Unlock Phones: Yes</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Talk Time: 4-6h</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Battery Type: Not Detachable</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Size: 154.6x75.2x8.35mm</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Display Resolution: 1920x1080</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Battery Capacity(mAh): 4000mAh</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>CPU Manufacturer: Qualcomm</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>Feature: Gravity Response,MP3 Playback,Touchscreen,GPS</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                    <p>CPU: Octa Core</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>ROM: 16G</p>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-6">
                                                    <p>SlotsDesign: Bar</p>
                                                </div>
                                            </li>
                                        </ul>
										<div class="compare-details-price">
											<a href="https://www.snapdeal.com/" target="_blank" class="btn btn-primary btn-sm compare-details-btn">
                                                <div class="d-flex justify-content-between">
    												<p class="pull-left">Buy now</p>
    												<img src="{{URL::to('img/compare/btn-img-5.png')}}" class="figure-img img-fluid" alt="compare">
    												<p class="pull-right">$299</p>
                                                </div>
											</a>
										</div>


                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="wd-accordion display-none-md">
                      <div class="panel panel-default">
                        <div class="panel-heading" data-acc-link="demo1">Body type <i class="fa fa-caret-down" aria-hidden="true"></i></div>
                        <div class="panel-body acc-open" data-acc-content="demo1">
                            <div id="no-more-tables">
                                <table class="col-md-12 p0 table table-responsive">
                            		<tbody>
                            			<tr>
                            				<td>Body type</td>
                            				<td>Mid-size SLR</td>
                            				<td>Mid-size SLR</td>
                            				<td>Mid-size SLR</td>
                            				<td>Mid-size SLR</td>
                            				<td>Mid-size SLR</td>
                            			</tr>
                            			<tr>
                            				<td>Body material</td>
                            				<td>Carbon fiber, composite</td>
                            				<td>Carbon fiber, composite</td>
                            				<td>Carbon fiber, composite</td>
                            				<td>Carbon fiber, composite</td>
                            				<td>Carbon fiber, composite</td>
                            			</tr>
                            		</tbody>
                            	</table>
                            </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading" data-acc-link="demo2">Sensor <i class="fa fa-caret-down" aria-hidden="true"></i></div>
                        <div class="panel-body acc-open" data-acc-content="demo2">
                            <div id="no-more-tables1">
                                <table class="col-md-12 p0 table table-responsive">
                            		<tbody>
                            			<tr>
                            				<td>Max resolution </td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            			</tr>
                            			<tr>
                            				<td>Other resolutions</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            			</tr>
                            		</tbody>
                            	</table>
                            </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading" data-acc-link="demo3">Description <i class="fa fa-caret-down" aria-hidden="true"></i></div>
                        <div class="panel-body acc-open" data-acc-content="demo3">
                            <div id="no-more-tables2">
                                <table class="col-md-12 p0 table table-responsive">
                            		<tbody>
                            			<tr>
                            				<td>Effective pixels</td>
                            				<td>12 megapixels</td>
                            				<td>12 megapixels</td>
                            				<td>12 megapixels</td>
                            				<td>12 megapixels</td>
                            				<td>12 megapixels</td>
                            			</tr>
                            			<tr>
                            				<td>Sensor photo detect</td>
                            				<td>13 megapixels</td>
                            				<td>13 megapixels</td>
                            				<td>13 megapixels</td>
                            				<td>13 megapixels</td>
                            				<td>13 megapixels</td>
                            			</tr>
                            			<tr>
                            				<td>Sensor size</td>
                            				<td>APS-C (23.6 x 15.8 mm)</td>
                            				<td>APS-C (23.6 x 15.8 mm)</td>
                            				<td>APS-C (23.6 x 15.8 mm)</td>
                            				<td>APS-C (23.6 x 15.8 mm)</td>
                            				<td>APS-C (23.6 x 15.8 mm)</td>
                            			</tr>
                            			<tr>
                            				<td>Processor</td>
                            				<td>Expeed</td>
                            				<td>Expeed</td>
                            				<td>Expeed</td>
                            				<td>Expeed</td>
                            				<td>Expeed</td>
                            			</tr>
                            			<tr>
                            				<td>Processor</td>
                            				<td>sRGB, Adobe RGB</td>
                            				<td>sRGB, Adobe RGB</td>
                            				<td>sRGB, Adobe RGB</td>
                            				<td>sRGB, Adobe RGB</td>
                            				<td>sRGB, Adobe RGB</td>
                            			</tr>
                            		</tbody>
                            	</table>
                            </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading" data-acc-link="demo4">Storage <i class="fa fa-caret-down" aria-hidden="true"></i></div>
                        <div class="panel-body acc-open" data-acc-content="demo4">
                            <div id="no-more-tables3">
                                <table class="col-md-12 p0 table table-responsive">
                            		<tbody>
                            			<tr>
                            				<td>Storage</td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            			</tr>
                            			<tr>
                            				<td>Screen / viewfinder</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            			</tr>
                            		</tbody>
                            	</table>
                            </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading" data-acc-link="demo5">Screen / viewfinder <i class="fa fa-caret-down" aria-hidden="true"></i></div>
                        <div class="panel-body acc-open" data-acc-content="demo5">
                            <div id="no-more-tables4">
                                <table class="col-md-12 p0 table table-responsive">
                            		<tbody>
                            			<tr>
                            				<td>Max resolution </td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            				<td>4288 x 2848</td>
                            			</tr>
                            			<tr>
                            				<td>Other resolutions</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            				<td>3216 x 2136, 2144 x 1424</td>
                            			</tr>
                            		</tbody>
                            	</table>
                            </div>
                        </div>
                      </div>
                    </div>
				</div>
			</div>

    	</div>
    </section>
    @endsection