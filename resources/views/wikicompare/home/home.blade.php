@extends('layouts.frontend_default')
@section('content')
    <section id="main-slider-section">
    	<div id="main-slider" class="slider-bg2  owl-carousel owl-theme product-review slider-cat">
			<div class="item d-flex  slider-bg align-items-center">
				<div class="container-fluid">
					<div class="row justify-content-end">
					
						<div class="slider-text col-sm-6  col-xl-4   col-md-6 order-2 order-sm-1">
							<h6 class="sub-title">Choose your favourite market</h6>
							<h1 class="slider-title"><strong class="highlights-text">Compare</strong> Best Prices</h1>
							<p class="slider-content">Grabe it hurry.</p>
							<a href="#" class="btn btn-primary wd-shop-btn slider-btn">
								Go to store <i class="fa fa-arrow-right" aria-hidden="true"></i>
							</a>
						</div>
						<div class="col-sm-6  col-md-6 col-xl-6 order-1 order-sm-2 slider-img">
						    <img src="{{URL::to('img/slider-img/slider3.png')}}" alt="">
						</div>
					</div>
				</div>
			</div>
           <div class="item d-flex  slider-bg align-items-center">
				<div class="container-fluid">
					<div class="row justify-content-end">
						<div class="slider-text  col-sm-6 col-xl-4    col-md-6">
							<h6 class="sub-title">Choose your favourite market</h6>
							<h1 class="slider-title"><strong class="highlights-text">Compare</strong> Best Prices</h1>
							<p class="slider-content">Grabe it hurry.</p>
							<a href="#" class="btn btn-primary wd-shop-btn slider-btn">
								Go to store <i class="fa fa-arrow-right" aria-hidden="true"></i>
							</a>
						</div>
						<div class="col-sm-6 col-md-6 col-xl-6  slider-img fadeInDown animated">
					
						</div>
					</div>
				</div>
			</div>
    	</div>
    </section>

	<!-- =========================
        Choose Category
    ============================== -->
    <section id="choose-category">
    	<div class="container-fluid custom-width">
    		<div class="choose-category-area-box">
	    		<div class="row">
					<div class="col-md-12 text-center">
						<h2 class="choose-category-main-title">Recent Product Comparison</h2>
					</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="300ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-4.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #ff8a80"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="600ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-5.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #8899f9"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="900ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-6.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #84ffff"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="1200ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-7.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #ff9e80"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-8.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #ea80fc"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-9.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #84ffff"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-12.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #ffd740"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-13.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #a788ff"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-14.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #ff8a80"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-15.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #8c9eff"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-16.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #84ffff"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-17.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #ff9e80"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-18.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #ea80fc"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-19.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #90a4ae"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-6.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #98e389"></span>
	    			</div>
	    			<div class="col-sm-6 col-md-4 col-lg-3 choose-category-box wow fadeIn animated" data-wow-delay="0ms">
						<div class="d-flex justify-content-start align-items-center">
		    				<div class="category-info">
								<h6 class="choose-category-title">Laptops</h6>
	    						<div class="choose-category-content">
									<p>Compare and buy best Branded
									laptops from online store</p>
	    						</div>
	    						<a href="#" class="badge badge-light choose-category-link">Visit Category</a>
		    				</div>
		    				<div class="category-img">
		    					<a href="#"><img class="img-fluid" src="{{URL::to('img/categories/categories-img-14.jpg')}}" alt=""></a>
		    				</div>
						</div>
						<span class="wd-border-bottom" style="background: #a788ff"></span>
	    			</div>
	    		</div>
    		</div>
    	</div>
    </section>


	<!-- =========================
        Amazon Top Deals
    ============================== -->
	<section id="recent-product" class="recent-pro-2">
    	<div class="container-fluid custom-width">
    		<div class="row">
    			<div class="col-md-12 text-center">
    				<h2 class="recent-product-title">Recent Product Comparison</h2>
    			</div>

				<div class="col-md-12">
					<div class="row">
						<div class="col-12 col-md-12 col-lg-4 col-xl-2">
							<div class="wd-amazon-product">
								<!-- <h4>Amazon Top Deals</h4> -->
							</div>
						</div>
						<div class="col-md-12 col-lg-8">
							<ul class="nav nav-tabs wd-amazon-product-tabs " id="myTab-recent" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-expanded="true">Market 1</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="ebay-tab" data-toggle="tab" href="#ebay" role="tab" aria-controls="ebay">Market 2</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="Elipkart-tab" data-toggle="tab" href="#Elipkart" role="tab" aria-controls="Elipkart">Market 3</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="aliexpress-tab" data-toggle="tab" href="#aliexpress" role="tab" aria-controls="aliexpress">Market 4</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="alibaba-tab" data-toggle="tab" href="#alibaba" role="tab" aria-controls="alibaba">Market 5</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="tab-content" id="myTabContent-recent">
						<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
							<div class="wd-amazon-product tab-inner-title">
								<h4>Market 1 Top Deals</h4>
							</div>
							<div class="row">
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">												
												<img src="{{URL::to('img/product-img/th-product-img-1.jpg')}}" class="img-fluid" alt="recent-product img">
												<div class="recent-product-overlay text-center">
													<div class="compare-btn">
														<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
													</div>
													<div class='countdown' data-date="2018-12-31"></div>
												</div>
											</div>

				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6  col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-2.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-3.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-4.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-2.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-8.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-9.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-11.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-11.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-1.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-2.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-3.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-4.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-4.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
							</div>
						</div>
						<div class="tab-pane fade" id="ebay" role="tabpanel" aria-labelledby="ebay-tab">
							<div class="wd-amazon-product tab-inner-title">
								<h4>Market 2 Top Deals</h4>
							</div>
							<div class="row">
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>

				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-6.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-11.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-6.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-8.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-9.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-10.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-11.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-1.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-2.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-3.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-4.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-6.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
							</div>
						</div>
						<div class="tab-pane fade" id="Elipkart" role="tabpanel" aria-labelledby="Elipkart-tab">
							<div class="wd-amazon-product tab-inner-title">
								<h4>Market 3 Top Deals</h4>
							</div>
							<div class="row">
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-1.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>

				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-2.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-3.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-4.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-6.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-8.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-9.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-10.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-11.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-1.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-2.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-3.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-4.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-6.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
							</div>
						</div>
						<div class="tab-pane fade" id="aliexpress" role="tabpanel" aria-labelledby="aliexpress-tab">
							<div class="wd-amazon-product tab-inner-title">
								<h4>Market 4 Top Deals</h4>
							</div>
							<div class="row">
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>

				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-6.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-11.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-6.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-8.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-9.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-10.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-11.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-1.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-2.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-3.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-4.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-6.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
							</div>
						</div>
						<div class="tab-pane fade" id="alibaba" role="tabpanel" aria-labelledby="alibaba-tab">
							<div class="wd-amazon-product tab-inner-title">
								<h4>Market 5 Top Deals</h4>
							</div>
							<div class="row">
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>

				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-6.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-11.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-6.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-8.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-9.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-10.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-11.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-1.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-2.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-3.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-4.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-5.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-6.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
				    			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
				    				<div class="recent-product-box wow fadeInUp animated" data-wow-delay="0ms">
				    					<div class="recent-product-img">
											<div class="product-img">
												
													<img src="{{URL::to('img/product-img/th-product-img-7.jpg')}}" class="img-fluid" alt="recent-product img">
													<div class="recent-product-overlay text-center">
														<div class="compare-btn">
															<button type="button" class="btn btn-primary btn-sm third-home-quick-viwe" data-toggle="modal" data-target=".bd-example-modal-lg-product-1"><i class="fa fa-eye" aria-hidden="true"></i> Quick view</button>
														</div>
														<div class='countdown' data-date="2018-12-31"></div>
													</div>
												
											</div>
				    						<span class="badge badge-secondary wd-badge text-uppercase">New</span>
				    						<div class="recent-product-info">
					    						<div class="d-flex justify-content-between">
					    							<div class="recent-price">
					    								$40.30 - $40.30
					    							</div>
					    							<div class="recente-product-categories">
					    								Phones
					    							</div>
					    						</div>
					    						<div class="recente-product-content">
													<a href="product-details.html">Phone 550 and  Scorebox  review example</a>
					    						</div>
				    							<div class="recent-product-meta-link">
				    								<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i><strong>4.5</strong></a>
				    								<a href="#">
				    									<img src="{{URL::to('img/product-img/compare.png')}}" alt="">
				    									<img class="compare-white" src="{{URL::to('img/product-img/compare-white.png')}}" alt="">
				    								</a>
				    								<a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>145</a>
				    							</div>
				    						</div>
				    					</div>
				    				</div>
				    			</div>
							</div>
						</div>
					</div>
				</div>
    		</div>
    	</div>
    </section>
	<div class="product-view-modal modal fade bd-example-modal-lg-product-1" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

		    <!-- ====================================
		        Product Details Gallery Section
		    ========================================= -->
			<div class="row">
				<div class="product-gallery col-12 col-md-12 col-lg-6">
				    <!-- ====================================
				        Single Product Gallery Section
				    ========================================= -->
				    <div class="row">
						<div class="col-md-12 product-slier-details">
							<div id="product-view-model" class="product-view owl-carousel owl-theme">
							    <div class="item">
							    	<img src="{{URL::to('img/product-img/product-view-img-1.jpg')}}" class="img-fluid" alt="Product Img">
							    </div>
							    <div class="item">
							    	<img src="{{URL::to('img/product-img/product-view-img-2.jpg')}}" class="img-fluid" alt="Product Img">
							    </div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-6 col-12 col-md-12 col-lg-6">
					<div class="product-details-gallery">
						<div class="list-group">
							<h4 class="list-group-item-heading product-title">
								Vigo SP111-31N-P2GH Spin 1
							</h4>
							<div class="media">
								<div class="media-left media-middle">
									<div class="rating">
										<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
										<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
										<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
										<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
									</div>
								</div>
								<div class="media-body">
									<p>3.7/5 <span class="product-ratings-text"> -1747 Ratings</span></p>
								</div>
							</div>
						</div>
						<div class="list-group content-list">
							<p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> 100% Original product</p>
							<p><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Manufacturer Warranty</p>
						</div>
					</div>
					<div class="product-store row">
						<div class="col-12 product-store-box">
							<div class="row">
								<div class="col-3 p0 store-border-img">
									<img src="{{URL::to('img/product-store/product-store-img1.jpg')}}" class="figure-img img-fluid" alt="Product Img">
								</div>
								<div class="col-5 store-border-price text-center">
									<div class="price">
										<p>$234</p>
									</div>
								</div>
								<div class="col-4 store-border-button">
									<a href="#" class="btn btn-primary wd-shop-btn pull-right">
										Buy it now
									</a>
								</div>
							</div>
						</div>
						<div class="col-12 product-store-box">
							<div class="row">
								<div class="col-3 p0 store-border-img">
									<img src="{{URL::to('img/product-store/product-store-img2.jpg')}}" class="figure-img img-fluid" alt="Product Img">
								</div>
								<div class="col-5 store-border-price text-center">
									<div class="price">
										<p>$535</p>
									</div>
								</div>
								<div class="col-4 store-border-button">
									<a href="#" class="btn btn-primary wd-shop-btn pull-right red-bg">
										Buy it now
									</a>
								</div>
							</div>
						</div>
						<div class="col-12 product-store-box">
							<div class="row">
								<div class="col-3 p0 store-border-img">
									<img src="{{URL::to('img/product-store/product-store-img3.jpg')}}" class="figure-img img-fluid" alt="Product Img">
								</div>
								<div class="col-5 store-border-price">
									<span class="badge badge-secondary wd-badge text-uppercase">Best</span>
									<div class="price text-center">
										<p>$198</p>
									</div>
								</div>
								<div class="col-4 store-border-button">
									<a href="#" class="btn btn-primary wd-shop-btn pull-right orange-bg">
										Buy it now
									</a>
								</div>
							</div>
						</div>
						<div class="col-12 product-store-box">
							<div class="row">
								<div class="col-3 p0 store-border-img">
									<img src="{{URL::to('img/product-store/product-store-img4.jpg')}}" class="figure-img img-fluid" alt="Product Img">
								</div>
								<div class="col-5 store-border-price text-center">
									<div class="price">
										<p>$634</p>
									</div>
								</div>
								<div class="col-4 store-border-button">
									<a href="#" class="btn btn-primary wd-shop-btn pull-right green-bg">
										Buy it now
									</a>
								</div>
							</div>
						</div>
						<div class="col-12 product-store-box">
							<div class="row">
								<div class="col-3 p0 store-border-img">
									<img src="{{URL::to('img/product-store/product-store-img5.jpg')}}" class="figure-img img-fluid" alt="Product Img">
								</div>
								<div class="col-5 store-border-price text-center">
									<div class="price">
										<p>$234</p>
									</div>
								</div>
								<div class="col-4 store-border-button">
									<a href="#" class="btn btn-primary wd-shop-btn pull-right blue-bg">
										Buy it now
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>



			</div>
		</div>
	</div>


 

	<!-- =========================
        Best Rated Items
    ============================== -->
    <section id="best-rated">
    	<div class="container-fluid custom-width">
    		<div class="row">
    			<div class="col-md-12">
    				<div class="text-left best-rated-title">
    					<h4>Best Rated Items</h4>
    				</div>
    				<div class="col-md-12 wow fadeInUp animated" data-wow-delay="300ms">
    				<div class="row">
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img-2.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img-3.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img-4.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img-5.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img-6.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img-7.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img-8.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img-9.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img-10.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img-11.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
	    				<div class="col-12 col-sm-6 col-md-6 col-xl-3 p0">
	    					<div class="bast-rated-box">
								<div class="media">
									<a href="product-details.html"><img class="d-flex mr-3" src="{{URL::to('img/best-rated/best-reted-img-12.jpg')}}" alt="Generic placeholder image"></a>
									<div class="media-body">
										<strong class="price">$50.00 - $60.00</strong>
										<p class="rated-content">Echo Dot ( 2nd Generation )</p>
										<div class="rating">
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star active-color" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
											<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
	    					</div>
	    				</div>
    				</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

	<!-- =========================
        Weekly Top News
    ============================== -->
    <section id="weekly-news">
    	<div class="container-fluid custom-width">
    		<div class="row">
    		    <div class="col-md-12 text-center">
					<h2 class="news-title">Weekly Top News</h2>
				</div>
    			<div class=" col-sm-6 col-md-6 col-xl-3 wow fadeInRight animated" data-wow-delay="300ms">
    				<div class="weekly-news-box">
						<figure class="figure">
							<div class="weekly-news-img text-left">
								<img src="{{URL::to('img/weekly-news/weekly-news-img-1.jpg')}}" class="figure-img img-fluid rounded" alt="weekly-news-img">
								<div class="weekly-news-title">
									<a href="single-blog-with.html"><h4>Most wonderfull affiliate market theme and template BLURB</h4></a>
								</div>
							</div>
							<figcaption class="figure-caption">
								<div class="blog-meta container">
									<div class="row">
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Likes  115</a>
										</div>
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-commenting" aria-hidden="true"></i> Comments  59</a>
										</div>
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share  20</a>
										</div>
									</div>
								</div>
								<div class="text-center">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum at, quibusdam ipsa tenetur possimus dignissimos vitae nesciunt laudantium laborum in iste eveniet reprehenderit maxime fVigoe culpa 
								</div>
								<a href="single-blog-with.html" class="badge badge-light wd-news-more-btn">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
							</figcaption>
						</figure>
						<div class="weekly-news-shape"></div>
    				</div>
    			</div>
    			<div class=" col-sm-6 col-md-6 col-xl-3 wow fadeInRight animated" data-wow-delay="600ms">
    				<div class="weekly-news-box">
						<figure class="figure">
							<div class="weekly-news-img text-left">
								<img src="{{URL::to('img/weekly-news/weekly-news-img-2.jpg')}}" class="figure-img img-fluid rounded" alt="weekly-news-img">
								<div class="weekly-news-title">
									<a href="single-blog-with.html"><h4>Power is your hand if you get money making theme </h4></a>
								</div>
							</div>
							<figcaption class="figure-caption">
								<div class="blog-meta container">
									<div class="row">
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Likes  115</a>
										</div>
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-commenting" aria-hidden="true"></i> Comments  59</a>
										</div>
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share  20</a>
										</div>
									</div>
								</div>
								<div class="text-center">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum at, quibusdam ipsa tenetur possimus dignissimos vitae nesciunt laudantium laborum in iste eveniet reprehenderit maxime fVigoe culpa 
								</div>
								<a href="single-blog-with.html" class="badge badge-light wd-news-more-btn">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
							</figcaption>
						</figure>
						<div class="weekly-news-shape"></div>
    				</div>
    			</div>
    			<div class=" col-sm-6 col-md-6 col-xl-3 wow fadeInRight animated" data-wow-delay="900ms">
    				<div class="weekly-news-box">
						<figure class="figure">
							<div class="weekly-news-img text-left">
								<img src="{{URL::to('img/weekly-news/weekly-news-img-3.jpg')}}" class="figure-img img-fluid rounded" alt="weekly-news-img">
								<div class="weekly-news-title">
									<a href="single-blog-with.html"><h4>No need to develop online store if you have idea like BLURB</h4></a>
								</div>
							</div>
							<figcaption class="figure-caption">
								<div class="blog-meta container">
									<div class="row">
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Likes  115</a>
										</div>
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-commenting" aria-hidden="true"></i> Comments  59</a>
										</div>
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share  20</a>
										</div>
									</div>
								</div>
								<div class="text-center">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum at, quibusdam ipsa tenetur possimus dignissimos vitae nesciunt laudantium laborum in iste eveniet reprehenderit maxime fVigoe culpa 
								</div>
								<a href="single-blog-with.html" class="badge badge-light wd-news-more-btn">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
							</figcaption>
						</figure>
						<div class="weekly-news-shape"></div>
    				</div>
    			</div>
    			<div class=" col-sm-6 col-md-6 col-xl-3 wow fadeInRight animated" data-wow-delay="1200ms">
    				<div class="weekly-news-box">
						<figure class="figure">
							<div class="weekly-news-img text-left">
								<img src="{{URL::to('img/weekly-news/weekly-news-img-4.jpg')}}" class="figure-img img-fluid rounded" alt="weekly-news-img">
								<div class="weekly-news-title">
									<a href="single-blog-with.html"><h4>Money making is easy to get our themes with affiliate market idea</h4></a>
								</div>
							</div>
							<figcaption class="figure-caption">
								<div class="blog-meta container">
									<div class="row">
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Likes  115</a>
										</div>
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-commenting" aria-hidden="true"></i> Comments  59</a>
										</div>
										<div class="col blog-meta-box">
											<a href="single-blog-with.html"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share  20</a>
										</div>
									</div>
								</div>
								<div class="text-center">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum at, quibusdam ipsa tenetur possimus dignissimos vitae nesciunt laudantium laborum in iste eveniet reprehenderit maxime fVigoe culpa 
								</div>
								<a href="single-blog-with.html" class="badge badge-light wd-news-more-btn">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
							</figcaption>
						</figure>
						<div class="weekly-news-shape"></div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
    @endsection