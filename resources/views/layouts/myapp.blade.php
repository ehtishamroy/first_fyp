<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="Themesbox">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>WIKICOMPARES ADMIN</title>
    <!-- Fevicon -->
    <link rel="shortcut icon" href="{{ URL::to('/assets/images/wikicompareslogo.png') }}">
    <!-- Start css -->
    <!-- Switchery css -->
    <link href="{{ URL::to('/assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet">
    <!-- Apex css -->
    <link href="{{ URL::to('/assets/plugins/apexcharts/apexcharts.css')}}" rel="stylesheet">
    <!-- Slick css -->
    <link href="{{ URL::to('/assets/plugins/slick/slick.css')}}" rel="stylesheet">
    <link href="{{ URL::to('/assets/plugins/slick/slick-theme.css')}}" rel="stylesheet">
    <link href="{{ URL::to('/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::to('/assets/css/icons.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::to('/assets/css/flag-icon.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::to('/assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <!-- End css -->
</head>
<body class="vertical-layout">    
    <!-- Start Infobar Setting Sidebar -->
    <div id="infobar-settings-sidebar" class="infobar-settings-sidebar">
        <div class="infobar-settings-sidebar-head d-flex w-100 justify-content-between">
            <h4>Settings</h4><a href="javascript:void(0)" id="infobar-settings-close" class="infobar-settings-close"><img src="{{URL::to('/assets/images/svg-icon/close.svg')}}" class="img-fluid menu-hamburger-close" alt="close"></a>
        </div>
   
    </div>
    <div class="infobar-settings-sidebar-overlay"></div>
    <!-- End Infobar Setting Sidebar -->
    <!-- Start Containerbar -->
    <div id="containerbar">
        <!-- Start Leftbar -->
        <div class="leftbar">
            <!-- Start Sidebar -->
            <div class="sidebar">
                <!-- Start Logobar -->
                <div class="logobar">
                    <a href="index.html" class="logo logo-large"><img src="{{URL::to('/assets/images/wikicomparelogo.png')}}" class="img-fluid" alt="logo"></a>
                    <a href="index.html" class="logo logo-small"><img src="{{URL::to('/assets/images/wikicomparelogo.png')}}" class="img-fluid" alt="logo"></a>
                </div>
                <!-- End Logobar -->
                <!-- Start Navigationbar -->
                <div class="navigationbar">
                      <ul class="vertical-menu">
                        <li>
                            <a href="{{URL::to('/roles')}}">
                              <img src="{{URL::to('/assets/images/svg-icon/user.svg')}}" class="img-fluid" alt="dashboard"><span>ROLES & PERMISSIONS</span><i class=""></i>
                            </a>
                            {{-- <ul class="vertical-submenu">
                                <li><a href="{{URL::to('/roles')}}">ROLES</a></li>
                                <li><a href="{{URL::to('/permissions')}}">PERMISSIONS</a></li>
                            </ul> --}}
                        </li>
                            <li>
                            <a href="javaScript:void();">
                              <img src="{{URL::to('/assets/images/svg-icon/ecommerce.svg')}}" class="img-fluid" alt="dashboard"><span>ITEMS</span><i class="feather icon-chevron-right pull-right"></i>
                            </a>
                            <ul class="vertical-submenu">
                                <li><a href="{{URL::to('/add-item')}}">Add items</a></li>
                                <li><a href="{{URL::to('/list-item')}}">All Items</a></li>
                            </ul>
                        </li> 
                        
                        <li>
                            <a href="javaScript:void();">
                              <img src="{{URL::to('/assets/images/svg-icon/form_elements.svg')}}" class="img-fluid" alt="dashboard"><span>BLOG</span><i class="feather icon-chevron-right pull-right"></i>
                            </a>
                            <ul class="vertical-submenu">
                                <li><a href="{{URL::to('/add-post')}}">Add Post</a></li>
                                <li><a href="{{URL::to('/list-post')}}">All Post</a></li>
                            </ul>
                        </li> 

                                    <li>
                            <a href="javaScript:void();">
                              <img src="{{URL::to('/assets/images/svg-icon/advanced.svg')}}" class="img-fluid" alt="dashboard"><span>CATEGORIES</span><i class=""></i>
                            </a>
                                 <ul class="vertical-submenu">
                                <li><a href="{{URL::to('/add-category')}}">Add Category</a></li>
                                <li><a href="{{URL::to('/list-category')}}">All Category</a></li>
                            </ul>
                          
                        </li>           
                    </ul>
                </div>
                <!-- End Navigationbar -->
            </div>
            <!-- End Sidebar -->
        </div>
        <!-- End Leftbar -->
        <!-- Start Rightbar -->
        <div class="rightbar">
            <!-- Start Topbar Mobile -->
            <div class="topbar-mobile">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="mobile-logobar">
                            <a href="index.html" class="mobile-logo"><img src="{{URL::to('/assets/images/wikicomparelogo.png')}}" class="img-fluid" alt="logo"></a>
                        </div>
                        <div class="mobile-togglebar">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <div class="topbar-toggle-icon">
                                        <a class="topbar-toggle-hamburger" href="javascript:void();">
                                            <img src="{{URL::to('/assets/images/svg-icon/horizontal.svg')}}" class="img-fluid menu-hamburger-horizontal" alt="horizontal">
                                            <img src="{{URL::to('/assets/images/svg-icon/verticle.svg')}}" class="img-fluid menu-hamburger-vertical" alt="verticle">
                                         </a>
                                     </div>
                                </li>
                                <li class="list-inline-item">
                                    <div class="menubar">
                                        <a class="menu-hamburger" href="javascript:void();">
                                            <img src="{{URL::to('/assets/images/svg-icon/collapse.svg')}}" class="img-fluid menu-hamburger-collapse" alt="collapse">
                                            <img src="{{URL::to('/assets/images/svg-icon/close.svg')}}" class="img-fluid menu-hamburger-close" alt="close">
                                         </a>
                                     </div>
                                </li>                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Topbar -->
            <div class="topbar">
                <!-- Start row -->
                <div class="row align-items-center">
                    <!-- Start col -->
                    <div class="col-md-12 align-self-center">
                        <div class="togglebar">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <div class="menubar">
                                        <a class="menu-hamburger" href="javascript:void();">
                                           <img src="{{URL::to('/assets/images/svg-icon/collapse.svg')}}" class="img-fluid menu-hamburger-collapse" alt="collapse">
                                           <img src="{{URL::to('/assets/images/svg-icon/close.svg')}}" class="img-fluid menu-hamburger-close" alt="close">
                                         </a>
                                     </div>
                                </li>
                                <li class="list-inline-item">
                                
                                </li>
                            </ul>
                        </div>
                        <div class="infobar">
                            <ul class="list-inline mb-0">
                            
                                <li class="list-inline-item">
                            
                                </li>                                
                                <li class="list-inline-item">
                                                                 
                                </li>
                                <li class="list-inline-item">
                                    <div class="profilebar">
                                        <div class="dropdown">
                                          <a class="dropdown-toggle" href="#" role="button" id="profilelink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{URL::to('assets/images/users/profile.svg')}}" class="img-fluid" alt="profile"><span class="feather icon-chevron-down live-icon"></span></a>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="profilelink">
                                                <div class="dropdown-item">
                                                    <div class="profilename">
                                                      <h5>{{Auth::getUser()->name }}</h5>
                                                    </div>
                                                </div>
                                                <div class="userbox">
                                                    <ul class="list-unstyled mb-0">
                                                        {{-- <li class="media dropdown-item">
                                                         
                                                        </li> --}}
                                                                                                               
                                                        <li class="media dropdown-item">
                                                            <a   href="{{ url('/logout') }}" class="profile-icon"><img src="{{URL::to('assets/images/svg-icon/logout.svg')}}" class="img-fluid" alt="logout">Logout</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End col -->
                </div> 
                <!-- End row -->
            </div>
            <!-- End Topbar -->
            <!-- Start Breadcrumbbar -->                    
            <div class="breadcrumbbar">
                <div class="row align-items-center">
                    <div class="col-md-8 col-lg-8">
                        <h4 class="page-title">WIKICOMPARES</h4>
                        <div class="breadcrumb-list">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">ADMIN</li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="widgetbar">
                            {{-- <button class="btn btn-primary-rgba"><i class="feather icon-plus mr-2"></i>Actions</button> --}}
                        </div>                        
                    </div>
                </div>          
            </div>
                        <div class="contentbar">                

            <main class="py-4">
            @yield('content')
        </main>
        </div>
               <div class="footerbar">
                <footer class="footer">
                    <p class="mb-0">WIKICOMPARES ADMINPANEL- All Rights Reserved.</p>
                </footer>
            </div>
            <!-- End Footerbar -->
        </div>
        <!-- End Rightbar -->
    </div>
    <!-- End Containerbar -->
    <!-- Start js -->        
    <script src="{{ URL::to('/assets/js/jquery.min.js')}}"></script>
    <script src="{{ URL::to('/assets/js/popper.min.js')}}"></script>
    <script src="{{ URL::to('/assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ URL::to('/assets/js/modernizr.min.js')}}"></script>
    <script src="{{ URL::to('/assets/js/detect.js')}}"></script>
    <script src="{{ URL::to('/assets/js/jquery.slimscroll.js')}}"></script>
    <script src="{{ URL::to('/assets/js/vertical-menu.js')}}"></script>
    <!-- Switchery js -->
    <script src="{{ URL::to('/assets/plugins/switchery/switchery.min.js')}}"></script>
    <!-- Apex js -->
    <script src="{{ URL::to('/assets/plugins/apexcharts/apexcharts.min.js')}}"></script>
    <script src="{{ URL::to('/assets/plugins/apexcharts/irregular-data-series.js')}}"></script>    
    <!-- Slick js -->
    <script src="{{ URL::to('/assets/plugins/slick/slick.min.js')}}"></script>
    <!-- Custom Dashboard js -->   
    <script src="{{ URL::to('/assets/js/custom/custom-dashboard.js')}}"></script>
    <!-- Core js -->
    <script src="{{ URL::to('/assets/js/core.js')}}"></script>
    <!-- End js -->
</body>

<!-- Mirrored from themesbox.in/admin-templates/orbiter/html/semi-dark-vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 26 Dec 2019 08:39:40 GMT -->
</html>