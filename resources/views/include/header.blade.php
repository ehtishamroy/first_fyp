<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="Themesbox">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>WIKICOMPARES ADMIN</title>
    <!-- Fevicon -->
    <link rel="shortcut icon" href="{{ URL::to('/assets/images/wikicompareslogo.png') }}">
    <!-- Start css -->
    <!-- Switchery css -->
    <link href="{{ URL::to('/assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet">
    <!-- Apex css -->
    <link href="{{ URL::to('/assets/plugins/apexcharts/apexcharts.css')}}" rel="stylesheet">
    <!-- Slick css -->
    <link href="{{ URL::to('/assets/plugins/slick/slick.css')}}" rel="stylesheet">
    <link href="{{ URL::to('/assets/plugins/slick/slick-theme.css')}}" rel="stylesheet">

    <link href="{{ URL::to('/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::to('/assets/css/icons.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::to('/assets/css/flag-icon.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::to('/assets/css/style.css')}}" rel="stylesheet" type="text/css">

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> --}}
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>

    <!-- End css -->
</head>
<body class="vertical-layout">    
    <!-- Start Infobar Setting Sidebar -->
    <div id="infobar-settings-sidebar" class="infobar-settings-sidebar">
        <div class="infobar-settings-sidebar-head d-flex w-100 justify-content-between">
            <h4>Settings</h4><a href="javascript:void(0)" id="infobar-settings-close" class="infobar-settings-close"><img src="{{URL::to('/assets/images/svg-icon/close.svg')}}" class="img-fluid menu-hamburger-close" alt="close"></a>
        </div>
   
    </div>
    <div class="infobar-settings-sidebar-overlay"></div>
    <!-- End Infobar Setting Sidebar -->
    <!-- Start Containerbar -->
    <div id="containerbar">
        <!-- Start Leftbar -->
        <div class="leftbar">
            <!-- Start Sidebar -->
            <div class="sidebar">
                <!-- Start Logobar -->
                <div class="logobar">
                    <a href="index.html" class="logo logo-large"><img src="{{URL::to('/assets/images/wikicomparelogo.png')}}" class="img-fluid" alt="logo"></a>
                    <a href="index.html" class="logo logo-small"><img src="{{URL::to('/assets/images/wikicomparelogo.png')}}" class="img-fluid" alt="logo"></a>
                </div>
                <!-- End Logobar -->
                <!-- Start Navigationbar -->
                <div class="navigationbar">
                    <ul class="vertical-menu">
                        <li>
                            <a href="{{URL::to('/roles')}}">
                              <img src="{{URL::to('/assets/images/svg-icon/user.svg')}}" class="img-fluid" alt="dashboard"><span>ROLES & PERMISSIONS</span><i class=""></i>
                            </a>
                            {{-- <ul class="vertical-submenu">
                                <li><a href="{{URL::to('/roles')}}">ROLES</a></li>
                                <li><a href="{{URL::to('/permissions')}}">PERMISSIONS</a></li>
                            </ul> --}}
                        </li>
                            <li>
                            <a href="javaScript:void();">
                              <img src="{{URL::to('/assets/images/svg-icon/ecommerce.svg')}}" class="img-fluid" alt="dashboard"><span>ITEMS</span><i class="feather icon-chevron-right pull-right"></i>
                            </a>
                            <ul class="vertical-submenu">
                                <li><a href="{{URL::to('/add-item')}}">Add items</a></li>
                                <li><a href="{{URL::to('/list-item')}}">All Items</a></li>
                            </ul>
                        </li> 
                        
                        <li>
                            <a href="javaScript:void();">
                              <img src="{{URL::to('/assets/images/svg-icon/form_elements.svg')}}" class="img-fluid" alt="dashboard"><span>BLOG</span><i class="feather icon-chevron-right pull-right"></i>
                            </a>
                            <ul class="vertical-submenu">
                                <li><a href="{{URL::to('/add-post')}}">Add Post</a></li>
                                <li><a href="{{URL::to('/list-post')}}">All Post</a></li>
                            </ul>
                        </li> 

                                    <li>
                            <a href="javaScript:void();">
                              <img src="{{URL::to('/assets/images/svg-icon/advanced.svg')}}" class="img-fluid" alt="dashboard"><span>CATEGORIES</span><i class=""></i>
                            </a>
                                 <ul class="vertical-submenu">
                                <li><a href="{{URL::to('/add-category')}}">Add Category</a></li>
                                <li><a href="{{URL::to('/list-category')}}">All Category</a></li>
                            </ul>
                          
                        </li>           
                    </ul>
                </div>
                <!-- End Navigationbar -->
            </div>
            <!-- End Sidebar -->
        </div>
        <!-- End Leftbar -->
        <!-- Start Rightbar -->
        <div class="rightbar">
            <!-- Start Topbar Mobile -->
            <div class="topbar-mobile">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="mobile-logobar">
                            <a href="index.html" class="mobile-logo"><img src="{{URL::to('/assets/images/wikicomparelogo.png')}}" class="img-fluid" alt="logo"></a>
                        </div>
                        <div class="mobile-togglebar">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <div class="topbar-toggle-icon">
                                        <a class="topbar-toggle-hamburger" href="javascript:void();">
                                            <img src="{{URL::to('/assets/images/svg-icon/horizontal.svg')}}" class="img-fluid menu-hamburger-horizontal" alt="horizontal">
                                            <img src="{{URL::to('/assets/images/svg-icon/verticle.svg')}}" class="img-fluid menu-hamburger-vertical" alt="verticle">
                                         </a>
                                     </div>
                                </li>
                                <li class="list-inline-item">
                                    <div class="menubar">
                                        <a class="menu-hamburger" href="javascript:void();">
                                            <img src="{{URL::to('/assets/images/svg-icon/collapse.svg')}}" class="img-fluid menu-hamburger-collapse" alt="collapse">
                                            <img src="{{URL::to('/assets/images/svg-icon/close.svg')}}" class="img-fluid menu-hamburger-close" alt="close">
                                         </a>
                                     </div>
                                </li>                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Topbar -->
            <div class="topbar">
                <!-- Start row -->
                <div class="row align-items-center">
                    <!-- Start col -->
                    <div class="col-md-12 align-self-center">
                        <div class="togglebar">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <div class="menubar">
                                        <a class="menu-hamburger" href="javascript:void();">
                                           <img src="{{URL::to('/assets/images/svg-icon/collapse.svg')}}" class="img-fluid menu-hamburger-collapse" alt="collapse">
                                           <img src="{{URL::to('/assets/images/svg-icon/close.svg')}}" class="img-fluid menu-hamburger-close" alt="close">
                                         </a>
                                     </div>
                                </li>
                                <li class="list-inline-item">
                                
                                </li>
                            </ul>
                        </div>
                        <div class="infobar">
                            <ul class="list-inline mb-0">
                            
                                <li class="list-inline-item">
                            
                                </li>                                
                                <li class="list-inline-item">
                                                                 
                                </li>
                                <li class="list-inline-item">
                                    <div class="profilebar">
                                        <div class="dropdown">
                                          <a class="dropdown-toggle" href="#" role="button" id="profilelink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{URL::to('assets/images/users/profile.svg')}}" class="img-fluid" alt="profile"><span class="feather icon-chevron-down live-icon"></span></a>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="profilelink">
                                                <div class="dropdown-item">
                                                    <div class="profilename">
                                                      <h5>{{Auth::user()->name }}</h5>
                                                    </div>
                                                </div>
                                                <div class="userbox">
                                                    <ul class="list-unstyled mb-0">
                                                        {{-- <li class="media dropdown-item">
                                                         
                                                        </li> --}}
                                                                                                               
                                                        <li class="media dropdown-item">
                                                            <a   href="{{ url('/logout') }}" class="profile-icon"><img src="{{URL::to('assets/images/svg-icon/logout.svg')}}" class="img-fluid" alt="logout">Logout</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End col -->
                </div> 
                <!-- End row -->
            </div>
            <!-- End Topbar -->
      