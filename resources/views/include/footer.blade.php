
            <div class="footerbar">
                <footer class="footer">
                    <p class="mb-0">WIKICOMPARES ADMINPANEL- All Rights Reserved.</p>
                </footer>
            </div>
            <!-- End Footerbar -->
        </div>
        <!-- End Rightbar -->
    </div>
    <!-- End Containerbar -->
    <!-- Start js -->        

    <script src="{{ URL::to('/assets/js/popper.min.js')}}"></script>
    <script src="{{ URL::to('/assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ URL::to('/assets/js/modernizr.min.js')}}"></script>
    <script src="{{ URL::to('/assets/js/detect.js')}}"></script>
    <script src="{{ URL::to('/assets/js/jquery.slimscroll.js')}}"></script>
    <script src="{{ URL::to('/assets/js/vertical-menu.js')}}"></script>
    <!-- Switchery js -->
    <script src="{{ URL::to('/assets/plugins/switchery/switchery.min.js')}}"></script>
    <!-- Apex js -->
    <script src="{{ URL::to('/assets/plugins/apexcharts/apexcharts.min.js')}}"></script>
    <script src="{{ URL::to('/assets/plugins/apexcharts/irregular-data-series.js')}}"></script>    
    <!-- Slick js -->
    <script src="{{ URL::to('/assets/plugins/slick/slick.min.js')}}"></script>
    <!-- Custom Dashboard js -->   
    <script src="{{ URL::to('/assets/js/custom/custom-dashboard.js')}}"></script>
    <!-- Core js -->
    <script src="{{ URL::to('/assets/js/core.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
     <script type="text/javascript">
        $(document).ready(function() {
          $('.summernote').summernote();
        });
    </script>
    <!-- End js -->
</body>

<!-- Mirrored from themesbox.in/admin-templates/orbiter/html/semi-dark-vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 26 Dec 2019 08:39:40 GMT -->
</html>